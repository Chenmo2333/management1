/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50540
 Source Host           : localhost:3306
 Source Schema         : management

 Target Server Type    : MySQL
 Target Server Version : 50540
 File Encoding         : 65001

 Date: 28/07/2021 00:27:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_activity
-- ----------------------------
DROP TABLE IF EXISTS `sys_activity`;
CREATE TABLE `sys_activity`  (
  `id` int(16) NOT NULL COMMENT '活动编号',
  `userId` int(16) NULL DEFAULT NULL COMMENT '活动负责人id',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动描述',
  `beginTime` datetime NOT NULL COMMENT '开始时间',
  `endTime` datetime NOT NULL COMMENT '结束时间',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_activity
-- ----------------------------
INSERT INTO `sys_activity` VALUES (1, 1, 0, '中国软件杯软件设计大赛', '山上的蟑螂很大', '2021-05-12 21:05:16', '2021-05-12 21:04:43', '2021-05-12 21:04:47', '2021-05-12 21:04:51');
INSERT INTO `sys_activity` VALUES (2, 2, 0, '第七届中国国际“互联网+”大学生创新创业大赛重庆赛区选拔赛', '天冷适合睡觉', '2021-05-12 21:06:40', '2021-05-12 21:06:43', '2021-05-12 21:06:46', '2021-05-12 21:06:49');
INSERT INTO `sys_activity` VALUES (3, 3, 0, '青年大学习', '今夜局部有雨', '2021-05-12 21:06:40', '2021-05-12 21:06:43', '2021-05-12 21:06:46', '2021-05-12 21:06:49');
INSERT INTO `sys_activity` VALUES (4, 4, 0, '庆祝中国共产党成立100周年党史知识线上竞答', '天热会进入夏眠模式', '2021-05-12 21:06:40', '2021-05-12 21:06:43', '2021-05-12 21:06:46', '2021-05-12 21:06:49');

-- ----------------------------
-- Table structure for sys_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_group`;
CREATE TABLE `sys_group`  (
  `id` int(16) NOT NULL AUTO_INCREMENT COMMENT '组ID',
  `activityId` int(16) NULL DEFAULT NULL COMMENT '活动id',
  `groupName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组名',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '介绍',
  `volunteerList` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '志愿者列表',
  `memberList` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '普通成员列表',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime NOT NULL COMMENT '更新时间',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_group
-- ----------------------------
INSERT INTO `sys_group` VALUES (1, 1, '信息材料小组', '难写的存储过程', NULL, NULL, '2021-05-16 10:12:07', '2021-06-10 15:06:03', 0);
INSERT INTO `sys_group` VALUES (2, 1, '综合协调小组', '难写的SQL语句', NULL, NULL, '2021-05-16 10:12:07', '2021-05-16 10:12:07', 0);
INSERT INTO `sys_group` VALUES (3, 2, '食宿医保障', '（1）负责制定贵宾住宿、就餐、就医总体方案；（2）负责贵宾住宿酒店的洽谈预订、用餐及食宿费用结算', NULL, NULL, '2021-05-16 10:12:07', '2021-05-16 10:12:07', 0);
INSERT INTO `sys_group` VALUES (4, 2, '来宾接送小组', '难写的SQL语句', NULL, NULL, '2021-05-16 10:12:07', '2021-05-16 10:12:07', 0);
INSERT INTO `sys_group` VALUES (5, 2, '来宾报到接待小组', '难写的SQL语句', NULL, NULL, '2021-05-16 10:12:07', '2021-05-16 10:12:07', 0);
INSERT INTO `sys_group` VALUES (6, 1, '食宿医保障', '难写的SQL语句', NULL, NULL, '2021-05-28 11:20:37', '2021-05-28 11:20:39', 0);
INSERT INTO `sys_group` VALUES (7, 1, '来宾接送小组', '难写的前端代码', NULL, NULL, '2021-05-28 11:22:55', '2021-06-10 15:08:10', 0);
INSERT INTO `sys_group` VALUES (8, 1, '志愿服务小组', '难写的SQL语句', NULL, NULL, '2021-05-16 10:12:05', '2021-05-16 10:12:07', 0);
INSERT INTO `sys_group` VALUES (10, 4, '信息材料小组', '难写的前端代码', NULL, NULL, '2021-06-12 11:15:37', '2021-06-12 11:15:37', 0);
INSERT INTO `sys_group` VALUES (11, 1, '食宿医保障3', '是防辐射发送方式', NULL, NULL, '2021-06-24 11:01:52', '2021-06-24 11:01:52', 0);
INSERT INTO `sys_group` VALUES (12, 1, '食宿医保障124', '丰富的个', NULL, NULL, '2021-06-24 11:02:04', '2021-06-24 11:02:04', 0);

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(16) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `pid` int(16) NULL DEFAULT NULL COMMENT '所属上级',
  `type` tinyint(3) NULL DEFAULT 0 COMMENT '类型(1:菜单,2:按钮)',
  `permission_value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限值',
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问路径',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '名称',
  `component` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `meta` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态(0:禁止,1:正常)',
  `isDeleted` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `createTime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `hidden` tinyint(2) NULL DEFAULT NULL COMMENT '页面是否隐藏，0为隐藏，1为不隐藏',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, 0, 0, NULL, '', '全部数据', '', NULL, NULL, NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (2, 1, 1, '', '/user', '用户管理', 'Layout', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (3, 2, 1, NULL, 'table', '用户列表', '/management/user/getAllUser', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (4, 2, 1, NULL, 'save', '添加用户', '/management/user/save', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (5, 2, 2, 'user.edite', 'edit/:id', '编辑用户', '/management/user/save', NULL, 'table', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 0);
INSERT INTO `sys_permission` VALUES (6, 1, 1, NULL, '/role', '角色管理', 'Layout', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (7, 6, 1, NULL, 'table', '角色列表', '/management/role/getAllRole', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (8, 6, 1, NULL, 'save', '添加角色', '/management/role/save', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (9, 6, 1, NULL, 'edit/:id', '编辑角色', '/management/role/save', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (10, 1, 1, NULL, '/volunteer', '志愿者管理', 'Layout', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (11, 10, 1, NULL, 'table', '志愿者列表', '/management/volunteer/getAllVolunteer', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (12, 10, 1, NULL, 'save', '添加志愿者', '/management/volunteer/save', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (13, 10, 2, 'volunteer.edit', 'edit/:id', '编辑志愿者', '/management/volunteer/save', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (14, 1, 1, NULL, '/activity', '活动管理', 'Layout', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (15, 14, 1, NULL, 'table', '活动列表', '/management/activity/getAllActivities', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (16, 14, 1, NULL, 'save', '新增活动', '/management/activity/save', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (17, 14, 2, 'activity.edit', 'edit/:id', '编辑活动', '/management/activity/save', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (18, 14, 1, NULL, 'group/:id', '查看活动分组详情', '/management/group/getGroup', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (19, 14, 1, NULL, 'group', '活动分组管理', '/management/group/groupManage', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (20, 1, 1, NULL, '/task', '任务管理', 'Layout', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (21, 20, 1, NULL, 'list', '任务列表', '/management/task/taskList', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);
INSERT INTO `sys_permission` VALUES (22, 20, 1, NULL, 'distribute', '任务分配', '/management/task/taskDistribute', NULL, 'tree', NULL, 0, '2021-07-25 16:04:43', '2021-07-25 16:04:43', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'ADMIN', '终极管理员', '2017-05-01 13:25:39', '2021-05-12 20:36:15', 0);
INSERT INTO `sys_role` VALUES (2, 'USER', '普通用户', '2017-08-01 21:47:31', '2021-05-12 20:35:39', 0);
INSERT INTO `sys_role` VALUES (3, 'TEACHER', '本校教师', '2019-03-27 02:10:23', '2019-05-23 07:48:01', 0);
INSERT INTO `sys_role` VALUES (4, 'manageActivity', '活动负责人', '2019-04-29 02:16:48', '2021-05-12 20:34:18', 0);
INSERT INTO `sys_role` VALUES (5, 'groupLeader', '组长', '2021-05-16 10:36:13', '2021-05-16 10:36:15', 0);
INSERT INTO `sys_role` VALUES (6, 'deputyGroupLeader ', '副组长', '2021-05-16 10:37:51', '2021-05-16 10:38:02', 0);
INSERT INTO `sys_role` VALUES (7, 'executiveGruopLeader', '执行组长', '2021-05-16 10:41:23', '2021-05-16 10:41:25', 0);
INSERT INTO `sys_role` VALUES (8, 'groupMember', '组员', '2021-05-26 18:31:16', '2021-05-26 18:31:18', 0);

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `roleId` int(16) NULL DEFAULT NULL,
  `permissionId` int(16) NULL DEFAULT NULL,
  `isDeleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_sysrolepermission_permissionId`(`permissionId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (1, 1, 1, 0);
INSERT INTO `sys_role_permission` VALUES (2, 1, 2, 0);
INSERT INTO `sys_role_permission` VALUES (3, 1, 3, 0);
INSERT INTO `sys_role_permission` VALUES (4, 1, 4, 0);
INSERT INTO `sys_role_permission` VALUES (5, 1, 5, 0);
INSERT INTO `sys_role_permission` VALUES (6, 1, 6, 0);
INSERT INTO `sys_role_permission` VALUES (7, 1, 7, 0);
INSERT INTO `sys_role_permission` VALUES (8, 1, 8, 0);
INSERT INTO `sys_role_permission` VALUES (9, 1, 9, 0);
INSERT INTO `sys_role_permission` VALUES (10, 1, 10, 0);
INSERT INTO `sys_role_permission` VALUES (11, 1, 11, 0);
INSERT INTO `sys_role_permission` VALUES (12, 1, 12, 0);
INSERT INTO `sys_role_permission` VALUES (13, 1, 13, 0);
INSERT INTO `sys_role_permission` VALUES (14, 1, 14, 0);
INSERT INTO `sys_role_permission` VALUES (15, 1, 15, 0);
INSERT INTO `sys_role_permission` VALUES (16, 1, 16, 0);
INSERT INTO `sys_role_permission` VALUES (17, 1, 17, 0);
INSERT INTO `sys_role_permission` VALUES (18, 1, 18, 0);
INSERT INTO `sys_role_permission` VALUES (19, 1, 19, 0);
INSERT INTO `sys_role_permission` VALUES (20, 1, 20, 0);
INSERT INTO `sys_role_permission` VALUES (21, 1, 21, 0);
INSERT INTO `sys_role_permission` VALUES (22, 2, 1, 0);
INSERT INTO `sys_role_permission` VALUES (23, 2, 2, 0);
INSERT INTO `sys_role_permission` VALUES (24, 2, 3, 0);
INSERT INTO `sys_role_permission` VALUES (25, 2, 4, 0);
INSERT INTO `sys_role_permission` VALUES (80, 1, 22, 0);

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user`  (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `userId` int(16) NOT NULL,
  `roleId` int(16) NOT NULL DEFAULT 2,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `createTime` datetime NULL DEFAULT NULL,
  `updateTime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_roleId`(`roleId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES (1, 1, 1, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (3, 2, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (4, 2, 4, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (5, 3, 6, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (6, 4, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (7, 4, 8, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (8, 5, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (9, 5, 5, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (10, 6, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (11, 7, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (12, 7, 3, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (13, 8, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (14, 9, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (15, 10, 3, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (16, 11, 3, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (17, 12, 3, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (18, 13, 3, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (19, 14, 2, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (20, 14, 3, 0, NULL, NULL);
INSERT INTO `sys_role_user` VALUES (21, 20, 2, 0, NULL, NULL);

-- ----------------------------
-- Table structure for sys_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task`  (
  `id` int(16) NOT NULL AUTO_INCREMENT COMMENT '人物id',
  `pid` int(16) NULL DEFAULT NULL COMMENT '是否有父级任务，0表示无',
  `activityId` int(16) NULL DEFAULT NULL COMMENT '活动id',
  `groupId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组号',
  `sponsorId` int(16) NULL DEFAULT NULL COMMENT '发起人id',
  `executorId` int(16) NULL DEFAULT NULL COMMENT '执行人id',
  `taskName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `taskDescription` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '任务描述',
  `groupName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组别',
  `executorName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行人姓名',
  `sponsorName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人姓名',
  `taskStatus` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务状态',
  `beginTime` datetime NULL DEFAULT NULL COMMENT '任务开始时间',
  `endTime` datetime NULL DEFAULT NULL COMMENT '任务结束时间',
  `createTime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  `randomId` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '随机生成的任务id',
  `taskId` double NULL DEFAULT NULL COMMENT '按序生成的任务id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_task
-- ----------------------------
INSERT INTO `sys_task` VALUES (1, 0, 2, '5', 10, 12, '清洁', '周六下午两点半打扫会场', '来宾报到接待小组', '测试2', '李四2', '进行中', '2021-06-23 16:52:17', '2021-06-23 16:52:20', '2021-05-11 09:20:15', '2021-06-28 20:43:24', 0, '923147', 1);
INSERT INTO `sys_task` VALUES (2, 1, 2, '5', 10, 13, '扫地', '周六下午两点半打扫会场', '来宾报到接待小组', '呜呜呜', '李四2', '已完成', '2021-06-23 16:52:17', '2021-06-23 16:52:17', '2021-06-21 09:21:04', '2021-06-28 20:43:24', 0, '474325', 1.1);
INSERT INTO `sys_task` VALUES (3, 1, 2, '5', 10, 1, '拖地', '<p>周六下午两点半打扫<strong>会场</strong></p>', '来宾报到接待小组', '张三', '李四2', '进行中', '2021-06-23 16:52:17', '2021-06-23 16:52:17', '2021-06-21 09:21:32', '2021-06-28 20:43:24', 0, '058799', 1.2);
INSERT INTO `sys_task` VALUES (4, 0, 2, '4', 5, 3, '接人', '<p><strong>周六</strong>下午两点半打扫会场</p>', '来宾接送小组', '法外狂徒', '张三1', '进行中', '2021-06-23 16:52:17', '2021-06-23 16:52:17', '2021-06-21 09:22:02', '2021-06-28 20:43:24', 0, '915726', 2);
INSERT INTO `sys_task` VALUES (5, 2, 2, '5', 10, 10, '扫地', '周六下午两点半打扫二楼会场', '来宾报到接待小组', '李四2', '李四2', '已完成', '2021-06-23 16:52:17', '2021-06-23 16:52:17', '2021-06-21 09:23:25', '2021-07-27 19:23:19', 0, '874027', 1.11);
INSERT INTO `sys_task` VALUES (36, 4, 2, '4', 5, 2, '递四方速递', '粉色闪电发', '来宾接送小组', '李四', '张三1', '已过期', '2021-06-13 00:00:00', '2021-06-30 00:00:00', '2021-06-24 20:55:01', '2021-06-28 20:43:24', 0, '156054', 2.1);
INSERT INTO `sys_task` VALUES (38, 4, 2, '4', 5, 5, '所发生的', '很多很多人', '来宾接送小组', '张三1', '张三1', '未开始', '2021-06-20 00:00:00', '2021-06-28 00:00:00', '2021-06-24 20:55:48', '2021-06-28 20:43:24', 0, '755326', 2.2);
INSERT INTO `sys_task` VALUES (45, 0, 1, '1', 1, 6, '不复合地板', '<p>ry6</p>', '信息材料小组', '李四1', '张三', '进行中', '2021-06-14 00:00:00', '2021-06-28 00:00:00', '2021-06-27 10:56:57', '2021-06-28 20:43:24', 0, '076573', 3);
INSERT INTO `sys_task` VALUES (46, 45, 1, '1', 1, 6, '个人的', '<p>ry6u㔿</p>', '信息材料小组', '李四1', '张三', '进行中', '2021-06-14 00:00:00', '2021-06-30 00:00:00', '2021-06-27 10:57:23', '2021-06-28 20:43:24', 0, '787528', 3.1);
INSERT INTO `sys_task` VALUES (50, 46, 1, '1', 1, 6, '盾构施工', '<p>施工方大润发</p>', '信息材料小组', '李四1', '张三', '未开始', '2021-06-13 00:00:00', '2021-06-28 00:00:00', '2021-06-28 20:39:26', '2021-07-27 19:23:22', 0, '149791', 1.11);
INSERT INTO `sys_task` VALUES (52, 46, 1, '1', 1, 6, '是各色让她', '<p>光和热</p>', '信息材料小组', '李四1', '张三', '未开始', '2021-06-20 00:00:00', '2021-06-22 00:00:00', '2021-06-28 20:40:33', '2021-07-27 19:23:22', 0, '581475', 1.12);
INSERT INTO `sys_task` VALUES (53, 52, 1, '1', 1, 6, '隔热', '<p>光和热</p>', '信息材料小组', '李四1', '张三', '未开始', '2021-06-20 00:00:00', '2021-06-28 00:00:00', '2021-06-28 20:41:00', '2021-06-28 20:42:18', 1, '500876', 3.121);
INSERT INTO `sys_task` VALUES (54, 53, 1, '1', 1, 6, '更好的', '', '信息材料小组', '李四1', '张三', '未开始', '2021-06-20 00:00:00', '2021-06-28 00:00:00', '2021-06-28 20:42:18', '2021-06-28 20:42:18', 1, '057298', 3.1211);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(16) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '真实姓名',
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号密码',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `headImgUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像URL',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `telephone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `sex` tinyint(1) NULL DEFAULT NULL COMMENT '性别，1为男，0为女',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '账号状态，1为激活，0为未激活',
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '职位',
  `positionId` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '职工号、学号',
  `institution` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属单位、学院',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime NOT NULL COMMENT '更新时间',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'admin', '111', NULL, NULL, '123455432123', '134@qq.com', '2019-05-12', 1, 0, '副教授', 'S123542135', '重庆大学', '2019-05-14 04:44:22', '2021-06-01 17:12:11', 0);
INSERT INTO `sys_user` VALUES (2, 'user', '111111', '嗯嗯', NULL, NULL, '1111111111', '11@qq.com', '2019-03-31', 0, 0, '教师', 'S123546212', '重庆邮电大学', '2019-04-09 06:44:50', '2021-05-24 22:11:34', 0);
INSERT INTO `sys_user` VALUES (3, 'alex', '111111', '讲师', NULL, '', '13245698712', 'alex@qq.com', '2019-03-31', 1, 0, '教授', 'S123546215', '重庆邮电大学', '2019-03-27 02:27:35', '2021-05-10 09:10:27', 0);
INSERT INTO `sys_user` VALUES (4, '嗯嗯', '111111', 'user2', NULL, NULL, '09876567890', 'aa@QQ.com', '2019-05-12', 0, 0, '高级工程师', 'S125423684', '重庆邮电大学', '2019-05-15 02:22:21', '2019-05-21 00:57:14', 0);
INSERT INTO `sys_user` VALUES (5, 'user3', '111111', 'user3', NULL, NULL, '44366758876586578', 'bb@qq.com', '2019-05-14', 0, 0, '中级工程师', 'S568225541', '重庆大学', '2019-05-15 02:23:51', '2019-05-15 02:23:51', 0);
INSERT INTO `sys_user` VALUES (6, '王麻子', '111111', 'user4', NULL, NULL, '2143323543456876', 'cc@qq.com', '2019-04-30', 0, 0, '教师', 'S115416514', '重庆邮电大学', '2019-05-15 02:24:22', '2021-05-12 21:19:37', 0);
INSERT INTO `sys_user` VALUES (7, 'user5', '111111', 'user5', NULL, NULL, '1221344234565', 'dd@qq.com', '2018-12-03', 0, 0, '教师', 'S654156565', '重庆邮电大学', '2019-05-15 02:24:49', '2019-05-15 02:24:49', 0);
INSERT INTO `sys_user` VALUES (8, 'user6', '111111', 'user6', NULL, NULL, '123213215135453', 'ee@qq.coom', '2019-05-15', 0, 0, '校外教师', 'S154155454', '重庆邮电大学', '2019-05-15 02:25:16', '2019-05-21 03:08:26', 0);
INSERT INTO `sys_user` VALUES (9, 'user7', '111111', 'user7', NULL, NULL, '21345457980765', 'tt@qq.com', '2019-05-20', 0, 0, '校外教授', 'S154155414', '重庆邮电大学', '2019-05-15 06:16:32', '2019-05-21 03:08:37', 0);
INSERT INTO `sys_user` VALUES (10, 'user67', '111111', 'user67', NULL, NULL, '123456324568', 'asdsa@qq.com', '2019-05-14', 0, 0, '校外副教授', 'S114541552', '重庆邮电大学', '2019-05-16 08:39:11', '2019-05-16 08:39:11', 0);
INSERT INTO `sys_user` VALUES (11, 'string', '111111', 'string', 'string', 'string', 'string', 'string', '2019-02-01', 1, 0, '学生', 'S154151514', '重庆邮电大学', '2019-01-01 10:10:10', '2019-02-01 10:10:10', 0);
INSERT INTO `sys_user` VALUES (12, '张三', '111111', 'string', 'string', 'string', 'string', 'string', '2019-02-01', 1, 1, '学生', 'S154151455', '重庆大学', '2019-01-01 10:10:10', '2019-02-01 10:10:10', 0);
INSERT INTO `sys_user` VALUES (13, '李四', '111111', 'string', 'string', 'string', '13546324455', 'string', '2019-02-01', 1, 1, '博士', 'S125116546', '重庆大学', '2019-01-01 10:10:10', '2021-05-10 09:05:51', 0);
INSERT INTO `sys_user` VALUES (14, '滴滴滴', '111111', '滴滴滴', NULL, NULL, '154165165', NULL, NULL, 1, 0, '研究生', 'S165416541', '重庆大学', '2021-05-10 14:58:40', '2021-05-10 14:58:40', 0);

-- ----------------------------
-- Table structure for sys_user_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_group`;
CREATE TABLE `sys_user_group`  (
  `userId` int(16) NOT NULL COMMENT '用户id',
  `groupId` int(16) NOT NULL COMMENT '分组id',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_group
-- ----------------------------
INSERT INTO `sys_user_group` VALUES (1, 2, 0);
INSERT INTO `sys_user_group` VALUES (2, 5, 0);
INSERT INTO `sys_user_group` VALUES (3, 3, 0);
INSERT INTO `sys_user_group` VALUES (4, 3, 0);
INSERT INTO `sys_user_group` VALUES (5, 4, 0);

-- ----------------------------
-- Table structure for sys_volunteer
-- ----------------------------
DROP TABLE IF EXISTS `sys_volunteer`;
CREATE TABLE `sys_volunteer`  (
  `id` int(16) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `stuId` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学号',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '123456' COMMENT '密码',
  `headImgUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `telephone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `sex` tinyint(1) NULL DEFAULT 0 COMMENT '性别，0为男，1为女,默认为0',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '账号状态，0为未激活，1为激活，默认为0',
  `institution` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属学院',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime NOT NULL COMMENT '更新时间',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `authCode` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学号+姓名字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_volunteer
-- ----------------------------
INSERT INTO `sys_volunteer` VALUES (1, 'S1651619', '张三', '651616516', NULL, '5615156151', '1415454145@qq.com', 0, 1, '软件学院', '2021-05-13 19:30:57', '2021-06-26 16:24:37', 0, 'S1651619-张三');
INSERT INTO `sys_volunteer` VALUES (2, 'S564564', '李四', '651561161', NULL, '1541451515', '4115415@qq.com', 1, 1, '通信学院', '2021-05-13 19:34:31', '2021-06-26 16:24:37', 0, 'S564564-李四');
INSERT INTO `sys_volunteer` VALUES (3, 'S5616516516', '法外狂徒', '111116616', NULL, '141651651616165', '4651465146416@qq.com', 1, 0, '通信学院', '2021-05-13 22:55:39', '2021-06-26 16:24:37', 0, 'S5616516516-法外狂徒');
INSERT INTO `sys_volunteer` VALUES (4, 'S561611156', '测试', '111116616', NULL, '141651651616165', '465146416@qq.com', 1, 0, '通信学院', '2021-05-17 22:55:39', '2021-06-26 16:24:37', 0, 'S561611156-测试');
INSERT INTO `sys_volunteer` VALUES (5, 'S1651619', '张三1', '651616516', NULL, '5615156151', '1415454145@qq.com', 0, 1, '软件学院', '2021-05-13 19:30:57', '2021-06-26 16:24:37', 0, 'S1651619-张三1');
INSERT INTO `sys_volunteer` VALUES (6, 'S564564', '李四1', '651561161', NULL, '1541451515', '4115415@qq.com', 1, 1, '通信学院', '2021-05-13 19:34:31', '2021-06-26 16:24:37', 0, 'S564564-李四1');
INSERT INTO `sys_volunteer` VALUES (7, 'S5616516516', '法外狂徒1', '111116616', NULL, '141651651616165', '4651465146416@qq.com', 1, 1, '通信学院', '2021-05-13 22:55:39', '2021-06-26 16:24:37', 0, 'S5616516516-法外狂徒1');
INSERT INTO `sys_volunteer` VALUES (8, 'S561611156', '测试1', '111116616', NULL, '141651651616165', '465146416@qq.com', 1, 1, '通信学院', '2021-05-17 22:55:39', '2021-06-26 16:24:37', 0, 'S561611156-测试1');
INSERT INTO `sys_volunteer` VALUES (9, 'S1651619', '张三2', '651616516', NULL, '5615156151', '1415454145@qq.com', 0, 1, '软件学院', '2021-05-13 19:30:57', '2021-06-26 16:24:37', 0, 'S1651619-张三2');
INSERT INTO `sys_volunteer` VALUES (10, 'S564564', '李四2', '651561161', NULL, '1541451515', '4115415@qq.com', 1, 1, '通信学院', '2021-05-13 19:34:31', '2021-06-26 16:24:37', 0, 'S564564-李四2');
INSERT INTO `sys_volunteer` VALUES (11, 'S5616516516', '法外狂徒2', '111116616', NULL, '141651651616165', '4651465146416@qq.com', 1, 1, '通信学院', '2021-05-13 22:55:39', '2021-06-26 16:24:37', 0, 'S5616516516-法外狂徒2');
INSERT INTO `sys_volunteer` VALUES (12, 'S561611156', '测试2', '111116616', NULL, '141651651616165', '465146416@qq.com', 1, 1, '通信学院', '2021-05-17 22:55:39', '2021-06-26 16:24:37', 0, 'S561611156-测试2');
INSERT INTO `sys_volunteer` VALUES (13, 'S123453243', '呜呜呜', '123456', NULL, '12343212342', '124434322@qq.com', 0, 0, '计算机科学与技术', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S123453243-呜呜呜');
INSERT INTO `sys_volunteer` VALUES (14, 'S512564236', '刷拉拉番长', '123456', NULL, '12354126845', '1547895245@qq.com', 0, 1, '外语学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S512564236-刷拉拉番长');
INSERT INTO `sys_volunteer` VALUES (15, 'S123545685', '但丁', '123456', NULL, '12542598752', '1254789654@qq.com', 0, 1, '历史学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S123545685-但丁');
INSERT INTO `sys_volunteer` VALUES (16, 'S154789632', '维吉尔', '123456', NULL, '15151545622', '1515151566@qq.com', 0, 0, '加里敦学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S154789632-维吉尔');
INSERT INTO `sys_volunteer` VALUES (17, 'S125489632', '维尔', '123456', NULL, '15151552626', '255151515@qq.com', 0, 1, '加里敦学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S125489632-维尔');
INSERT INTO `sys_volunteer` VALUES (18, 'S125489643', '莱登', '123456', NULL, '15151552626', '255151515@qq.com', 0, 1, '加里敦学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S125489643-莱登');
INSERT INTO `sys_volunteer` VALUES (19, 'S125489542', '奥恩', '123456', NULL, '151515526452', '255151515@qq.com', 0, 1, '德玛西亚联盟院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S125489542-奥恩');
INSERT INTO `sys_volunteer` VALUES (20, 'S125429542', '游城十代', '123456', NULL, '151515521052', '255137515@qq.com', 0, 1, '决斗者学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S125429542-游城十代');
INSERT INTO `sys_volunteer` VALUES (21, 'S125429549', '不动游星', '123456', NULL, '151547521052', '227137515@qq.com', 0, 1, '决斗者学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S125429549-不动游星');
INSERT INTO `sys_volunteer` VALUES (22, 'S125435549', '明日香', '123456', NULL, '139547521052', '227137515@qq.com', 1, 1, '决斗者学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S125435549-明日香');
INSERT INTO `sys_volunteer` VALUES (23, 'S125432749', '霸王十代', '123456', NULL, '139548521052', '227137515@qq.com', 0, 1, '决斗者学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S125432749-霸王十代');
INSERT INTO `sys_volunteer` VALUES (24, 'S122832749', '吹雪', '123456', NULL, '139258521052', '226737515@qq.com', 1, 1, '决斗者学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S122832749-吹雪');
INSERT INTO `sys_volunteer` VALUES (25, 'S117832749', '城之内', '123456', NULL, '134658521052', '226731715@qq.com', 0, 1, '决斗者学院', '2021-05-17 22:33:33', '2021-06-26 16:24:37', 0, 'S117832749-城之内');

-- ----------------------------
-- Table structure for sys_volunteer_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_volunteer_group`;
CREATE TABLE `sys_volunteer_group`  (
  `volunteerId` int(16) NOT NULL COMMENT '志愿者ID',
  `groupId` int(16) NOT NULL COMMENT '组ID',
  `roleName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `roleId` int(16) NOT NULL COMMENT '角色id，1位组长，2为副组长，3为执行组长，4为普通组员，5为志愿者',
  `roleDescription` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `updateTime` datetime NOT NULL COMMENT '更新时间',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_volunteer_group
-- ----------------------------
INSERT INTO `sys_volunteer_group` VALUES (1, 5, '组员', 4, '组员', '2021-05-25 10:55:58', '2021-05-25 10:56:02', 0);
INSERT INTO `sys_volunteer_group` VALUES (2, 4, '组员', 4, '组员', '2021-05-25 10:56:04', '2021-05-25 10:56:07', 0);
INSERT INTO `sys_volunteer_group` VALUES (3, 4, '志愿者', 5, '志愿者', '2021-05-26 10:10:16', '2021-05-26 10:10:19', 0);
INSERT INTO `sys_volunteer_group` VALUES (4, 6, '志愿者', 5, '志愿者', '2021-05-26 10:10:16', '2021-05-26 10:10:16', 0);
INSERT INTO `sys_volunteer_group` VALUES (5, 4, '组长', 1, '组长', '2021-05-26 10:10:16', '2021-05-26 10:10:16', 0);
INSERT INTO `sys_volunteer_group` VALUES (6, 1, '组长', 1, '组长', '2021-05-28 11:25:19', '2021-05-28 11:25:21', 0);
INSERT INTO `sys_volunteer_group` VALUES (7, 2, '组长', 1, '组长', '2021-05-28 11:25:45', '2021-05-28 11:25:47', 0);
INSERT INTO `sys_volunteer_group` VALUES (8, 3, '组长', 1, '组长', '2021-05-28 11:26:00', '2021-05-28 11:26:02', 0);
INSERT INTO `sys_volunteer_group` VALUES (9, 4, '副组长', 2, '副组长', '2021-05-28 11:26:28', '2021-05-28 11:26:34', 0);
INSERT INTO `sys_volunteer_group` VALUES (10, 5, '组长', 1, '组长', '2021-05-28 11:27:38', '2021-05-28 11:27:42', 0);
INSERT INTO `sys_volunteer_group` VALUES (11, 6, '组长', 1, '组长', '2021-05-28 11:27:56', '2021-05-28 11:27:58', 0);
INSERT INTO `sys_volunteer_group` VALUES (12, 5, '组员', 4, '组员', '2021-05-29 15:06:58', '2021-05-29 15:07:00', 0);
INSERT INTO `sys_volunteer_group` VALUES (13, 5, '组员', 4, '组员', '2021-05-17 22:33:33', '2021-05-17 22:33:33', 0);
INSERT INTO `sys_volunteer_group` VALUES (14, 5, '组员', 4, '组员', '2021-05-17 22:33:33', '2021-05-17 22:33:33', 0);
INSERT INTO `sys_volunteer_group` VALUES (15, 5, '副组长', 2, '副组长', '2021-05-17 22:33:33', '2021-05-17 22:33:33', 0);

-- ----------------------------
-- Table structure for sys_volunteer_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_volunteer_task`;
CREATE TABLE `sys_volunteer_task`  (
  `id` int(16) NOT NULL COMMENT '人物id',
  `taskId` int(16) NULL DEFAULT NULL COMMENT '任务id',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_volunteer_task
-- ----------------------------
INSERT INTO `sys_volunteer_task` VALUES (1, 1, 0);
INSERT INTO `sys_volunteer_task` VALUES (2, 2, 0);
INSERT INTO `sys_volunteer_task` VALUES (3, 3, 0);
INSERT INTO `sys_volunteer_task` VALUES (4, 4, 0);
INSERT INTO `sys_volunteer_task` VALUES (5, 5, 0);
INSERT INTO `sys_volunteer_task` VALUES (6, 6, 0);
INSERT INTO `sys_volunteer_task` VALUES (7, 7, 0);
INSERT INTO `sys_volunteer_task` VALUES (8, 8, 0);
INSERT INTO `sys_volunteer_task` VALUES (9, 1, 0);

-- ----------------------------
-- View structure for viewgroupinfo
-- ----------------------------
DROP VIEW IF EXISTS `viewgroupinfo`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `viewgroupinfo` AS SELECT
                                                                              a.id AS activityId,
                                                                              a.name as activityName,
                                                                              g.id as groupId,
                                                                              g.groupName AS groupName,
                                                                              g.description,
                                                                              vg.volunteerId,
                                                                              v.username AS volunteerName ,
                                                                              vg.roleId,
                                                                              vg.roleName,
                                                                              vg.roleDescription
                                                                          FROM
                                                                              sys_activity a
                                                                                  LEFT JOIN sys_group g on g.activityId = a.id
                                                                                  LEFT JOIN sys_volunteer_group vg ON g.id = vg.groupId
                                                                                  LEFT JOIN sys_volunteer v ON v.id = vg.volunteerId ;

SET FOREIGN_KEY_CHECKS = 1;
