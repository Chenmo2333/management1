package com.management1.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.management1.commonutils.results;
import com.management1.entity.SysGroup;
import com.management1.entity.SysTask;
import com.management1.entity.SysVolunteerGroup;
import com.management1.service.SysGroupService;
import com.management1.service.SysTaskService;
import com.management1.service.SysVolunteerGroupService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@RestController
@RequestMapping("/group")
@CrossOrigin
public class SysGroupController {
    @Resource
    private SysGroupService groupService;

    @Resource
    private SysVolunteerGroupService volunteerGroupService;

    @Resource
    private SysTaskService taskService;

    @ApiOperation("获取所有分组信息")
    @GetMapping("getAllGroup")
    public results getAllGroup(){
        List<SysGroup> groupInfo = groupService.list();
        return results.ok().data("groupInfo",groupInfo);
    }

    @ApiOperation("增加新的分组")
    @PostMapping("addGroup")
    public results addGroup(@RequestBody SysGroup group){
        boolean save = groupService.save(group);
        if (save) {
            return results.ok();
        }else {
            return results.error("增加分组信息失败");
        }
    }

    @ApiOperation("通过组id修改组信息")
    @PostMapping("updateGroup")
    public results updateGroupInfo(@RequestBody SysGroup group){
        boolean b = groupService.updateById(group);
        if (b) {
            return results.ok();
        }else {
            return results.error("更新分组信息失败");
        }
    }

    @ApiOperation("删除分组，及分组下的所有成员")
    @DeleteMapping("deleteGroup/{groupId}")
    public results deleteGroup(@PathVariable Integer groupId){
        //1.删除组下的任务
        QueryWrapper<SysTask> taskQueryWrapper = new QueryWrapper<>();
        taskQueryWrapper.eq("groupId",groupId);
        taskService.remove(taskQueryWrapper);
        //2.删除组下的成员
        QueryWrapper<SysVolunteerGroup> volunteerGroupQueryWrapper = new QueryWrapper<>();
        volunteerGroupQueryWrapper.eq("groupId",groupId);
        volunteerGroupService.remove(volunteerGroupQueryWrapper);
        //3.删除组
        groupService.removeById(groupId);
        return results.ok();
    }

}

