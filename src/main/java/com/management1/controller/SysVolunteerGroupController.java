package com.management1.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.management1.commonutils.results;
import com.management1.entity.SysVolunteer;
import com.management1.entity.SysVolunteerGroup;
import com.management1.entity.activity.FirstActivity;
import com.management1.entity.dto.groupInfoViewDto;
import com.management1.entity.dto.roleDto;
import com.management1.entity.query.GroupQuery;
import com.management1.entity.vo.finalGroupInfo;
import com.management1.entity.vo.groupInfoVo;
import com.management1.mapper.SysVolunteerGroupMapper;
import com.management1.service.SysVolunteerGroupService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.schema.Entry;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@RestController
@RequestMapping("/volunteerGroup")
@CrossOrigin
public class SysVolunteerGroupController {
    @Resource
    private SysVolunteerGroupService volunteerGroupService;

    @Resource
    private SysVolunteerGroupMapper volunteerGroupMapper;

    @ApiOperation("获取分组信息，最终结果中的成员和志愿者以逗号分隔开")
    @GetMapping("groupInfoView/{activityId}")
    public results getGroupInfoView(@PathVariable Integer activityId) {
        List<finalGroupInfo> list = volunteerGroupService.getGroupInfoView(activityId);
        return results.ok().data("list", list);
    }

    @ApiOperation("获取分组信息，最终结果中的成员和志愿者以集合形式展现")
    @GetMapping("groupInfo")
    public results getGroupInfo() {
        List<groupInfoVo> groupInfo = volunteerGroupService.getGroupInfo();
        return results.ok().data("groupInfo", groupInfo);
    }

    @ApiOperation("直接展示志愿者和组员的分组信息")
    @PostMapping("groupManage/{current}/{limit}")
    public results groupManageByPage(@PathVariable long current, @PathVariable long limit,
                                     @RequestBody(required = false) GroupQuery groupQuery) {
        Page<groupInfoViewDto> pageCondition = new Page<>(current, limit);
        QueryWrapper<groupInfoViewDto> wrapper = new QueryWrapper<>();
        Integer activityId = groupQuery.getActivityId();
        Integer groupId = groupQuery.getGroupId();
        if (null != activityId)
            wrapper.eq("activityId", activityId);
//        wrapper.eq((null != activityId),"activityId", activityId);
        if (null != groupId)
            wrapper.eq("groupId", groupId);
        IPage<groupInfoViewDto> groupInfoByPage = volunteerGroupMapper.groupManage(pageCondition, wrapper);
        long total = groupInfoByPage.getTotal();
        List<groupInfoViewDto> records = groupInfoByPage.getRecords();
        return results.ok().data("total", total).data("rows", records);
    }

    @ApiOperation("查询每个活动下的分组信息")
    @GetMapping("activityGroupInfo")
    public results getActivityGroupInfo() {
        List<FirstActivity> activityGroupInfo = volunteerGroupService.getActivityGroupInfo();
        return results.ok().data("groupQuery", activityGroupInfo);
    }

    @ApiOperation("查询角色id和角色名")
    @GetMapping("getRoleList")
    public results getRoleList() {
        List<roleDto> roleList = volunteerGroupService.getRoleList();
        return results.ok().data("roleList", roleList);
    }

    @ApiOperation("添加组员到小组（单个添加）")
    @PostMapping("addMemberIntoGroup")
    public results addMemberIntoGroup(@RequestBody SysVolunteerGroup volunteerGroup) {
        boolean b = volunteerGroupService.addMemberIntoGroup(volunteerGroup);
        if (b)
            return results.ok();
        else
            return results.error("添加成员失败");
    }

    @ApiOperation("添加组员到小组（批量添加）")
    @PostMapping("addMemberListIntoGroup")
    public results addMemberListIntoGroup(@RequestBody List<SysVolunteerGroup> memberList) {
        if (null != memberList && memberList.size() > 0) {
            // 还应该考虑只选了角色，未选中人员的情况
            SysVolunteerGroup temp = memberList.get(0);
            Integer roleId = temp.getRoleId();
            String roleName = volunteerGroupMapper.getRoleName(roleId);
            // 批量给roleName赋值
            memberList.forEach(o -> o.setRoleName(roleName));
            boolean b = volunteerGroupService.saveBatch(memberList);
            if (b)
                return results.ok();
            else
                return results.error("批量添加成员失败");
        } else
            return results.error("请至少选中一个组员进行添加");
    }

    @ApiOperation("根据组id，查询小组内的人员，只查姓名和id值，用于分配任务")
    @GetMapping("getMemberByGroupId/{groupId}")
    public results getMemberByGroupId(@PathVariable Integer groupId){
        List<SysVolunteer> memberList = volunteerGroupService.getMemberByGroupId(groupId);
        return results.ok().data("groupMemberList",memberList);
    }

    @ApiOperation("将志愿者从小组中移除")
    @DeleteMapping("deleteVolunteerFromGroup/{volunteerId}")
    public results deleteVolunteerFromGroup(@PathVariable Integer volunteerId){
        QueryWrapper<SysVolunteerGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("volunteerId",volunteerId);
        volunteerGroupService.remove(wrapper);
        return results.ok();
    }
}

