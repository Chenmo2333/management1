package com.management1.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.management1.commonutils.RandomUtil;
import com.management1.commonutils.results;
import com.management1.entity.SysGroup;
import com.management1.entity.SysTask;
import com.management1.entity.query.TaskQuery;
import com.management1.service.SysGroupService;
import com.management1.service.SysTaskService;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chenmo
 * @since 2021-06-21
 */
@RestController
@RequestMapping("/task")
@CrossOrigin
public class SysTaskController {
    @Resource
    private SysTaskService taskService;

    @ApiOperation("获取所有任务信息，以树状形式展示,并生成任务id，当数据量大的时候性能估计会很差，因为，每次删除和新增任务都会将全表的数据更新一遍")
    @GetMapping("getAllTask")
    public results getAllTask() {
        List<SysTask> taskList = taskService.queryAllTask1();
        return results.ok().data("taskList", taskList);
    }

    @ApiOperation("测试获取所有任务信息，以树状形式展示,并生成任务id")
    @GetMapping("getAllTask1")
    public results getAllTask1() {
        List<SysTask> taskList = taskService.queryAllTask();
        return results.ok().data("taskList", taskList);
    }

    @ApiOperation("任务列表")
    @PostMapping("pageTaskCondition/{current}/{limit}")
    public results pageUserCondition(@PathVariable long current, @PathVariable long limit,
                                     @RequestBody(required = false) TaskQuery taskQuery) {
        Page<SysTask> page = new Page<>(current, limit);
        QueryWrapper<SysTask> taskWrapper = new QueryWrapper<>();
        Integer activityId = taskQuery.getActivityId();
        Integer groupId = taskQuery.getGroupId();
        String taskName = taskQuery.getTaskName();
        String taskStatus = taskQuery.getTaskStatus();
        if (null != activityId) {
            taskWrapper.eq("activityId", activityId);
        }
        if (null != groupId) {
            taskWrapper.eq("groupId", groupId);
        }
        if (!StringUtils.isEmpty(taskName)) {
            taskWrapper.like("taskName", taskName);
        }
//        taskWrapper.like(!StringUtils.isEmpty(taskName),"taskName", taskName);
        if (!StringUtils.isEmpty(taskStatus)) {
            taskWrapper.like("taskStatus", taskStatus);
        }
        taskService.page(page, taskWrapper);
        long total = page.getTotal();
        List<SysTask> records = page.getRecords();
        //对结果进行处理，使其变为树形结构
        List<SysTask> rows = taskService.buildTaskList1(records);
        if (null == rows || rows.size() == 0)
            return results.ok().data("total", total).data("rows", records);
        else
            return results.ok().data("total", total).data("rows", rows);
    }

    @ApiOperation("新增任务")
    @PostMapping("addTask")
    public results addTask(@RequestBody SysTask task) {
        task.setRandomId(RandomUtil.getSixBitRandom());
        boolean save = taskService.save(task);
        //更新所有任务的taskId
        taskService.queryAllTask();
        if (save) {
            return results.ok();
        } else {
            return results.error("新增任务失败");
        }
    }

    @ApiOperation("删除任务,需要递归删除子任务")
    @DeleteMapping("deleteTask/{taskId}")
    public results deleteTask(@PathVariable Integer taskId) {
        taskService.deleteTaskById(taskId);
        //更新所有任务的taskId
        taskService.queryAllTask();
        return results.ok();
    }

    @ApiOperation(value = "修改任务信息")
    @PutMapping("updateTaskInfo")
    public results updateTaskInfo(@RequestBody SysTask task) {
        boolean flag = taskService.updateById(task);
        if (flag) {
            return results.ok();
        } else {
            return results.error("更新任务信息失败");
        }
    }

    @ApiOperation(value = "查询所有的任务名称")
    @GetMapping("getAllTaskName")
    public results getAllTaskName() {
        List<SysTask> list = taskService.getAllTaskName();
        return results.ok().data("taskNameList", list);
    }

    @ApiOperation(value = "根据id，查询子任务的上一级任务")
    @GetMapping("getParentTaskName/{id}")
    public results getParentTaskName(@PathVariable Integer id){
        SysTask task = taskService.getParentTaskName(id);
        return results.ok().data("ParentTask",task);
    }

    @ApiOperation(value = "给任务赋予六位随机数")
    @GetMapping("setTaskRandomId")
    public results setTaskRandomId(){
        List<SysTask> list = taskService.list();
        list.forEach(item ->{
            item.setRandomId(RandomUtil.getSixBitRandom());
            taskService.updateById(item);
        });
        return results.ok();
    }
}

