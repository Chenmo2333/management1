package com.management1.controller;


import com.management1.commonutils.results;
import com.management1.entity.SysPermission;
import com.management1.service.SysPermissionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
@RestController
@RequestMapping("/permission")
public class SysPermissionController {
    @Resource
    private SysPermissionService permissionService;

    //获取全部菜单
    @ApiOperation(value = "查询所有菜单")
    @GetMapping
    public results indexAllPermission() {
        List<SysPermission> list =  permissionService.queryAllMenu();
        return results.ok().data("children",list);
    }

    @ApiOperation(value = "递归删除菜单")
    @DeleteMapping("remove/{id}")
    public results remove(@PathVariable Integer id) {
        permissionService.removeChildById(id);
        return results.ok();
    }

    @ApiOperation(value = "给角色分配权限")
    @PostMapping("/doAssign")
    public results doAssign(Integer roleId, Integer[] permissionId) {
        permissionService.saveRolePermissionRelationShip(roleId,permissionId);
        return results.ok();
    }

    @ApiOperation(value = "根据角色获取菜单")
    @GetMapping("toAssign/{roleId}")
    public results toAssign(@PathVariable Integer roleId) {
        List<SysPermission> list = permissionService.selectAllMenu(roleId);
        return results.ok().data("children", list);
    }

    @ApiOperation(value = "新增菜单")
    @PostMapping("save")
    public results save(@RequestBody SysPermission permission) {
        permissionService.save(permission);
        return results.ok();
    }

    @ApiOperation(value = "修改菜单")
    @PutMapping("update")
    public results updateById(@RequestBody SysPermission permission) {
        permissionService.updateById(permission);
        return results.ok();
    }

}

