package com.management1.controller;

import com.management1.commonutils.results;
import com.management1.entity.SysActivity;
import com.management1.entity.SysUser;
import com.management1.entity.dto.activityDto;
import com.management1.entity.vo.activityVo;
import com.management1.service.SysActivityService;
import com.management1.service.SysUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@RestController
@RequestMapping("/activity")
@CrossOrigin
public class SysActivityController {
    @Resource
    private SysActivityService activityService;

    @Resource
    private SysUserService userService;

    //获取活动列表，应当进一步处理数据，使表中包含任务完成度（完成/任务总数）、活动负责人姓名
    @ApiOperation("查询活动的基本信息")
    @GetMapping("/getAllActivities")
    public results getAllActivities() {
        List<SysActivity> activityList = activityService.list(null);
        List<activityDto> list = new LinkedList<>();
        for (SysActivity activity : activityList) {
            activityDto activityDto = new activityDto();
            String userId = activity.getUserId();
            SysUser user = userService.getById(userId);
            BeanUtils.copyProperties(activity, activityDto);
            if (userId != null)
                activityDto.setUserName(user.getUsername());
            list.add(activityDto);
        }
        return results.ok().data("list", list);
    }

    @ApiOperation("组合条件查询活动信息")
    @PostMapping("/pageActivityCondition/{current}/{limit}")
    public results pageActivityCondition(@PathVariable long current, @PathVariable long limit,
                                         @RequestBody(required = false) activityVo activityVo) {
        List<activityVo> list = activityService.getActivityByPage(current, limit, activityVo);
        int total = list.size();
        return results.ok().data("list", list).data("total", total);
    }

    //添加基本的活动信息，还要指定负责人，由负责人创建志愿者任务
    @ApiOperation("添加基本的活动信息")
    @PostMapping("/addActivity")
    public results addActivity(@RequestBody activityVo activityVo) {
        activityService.saveActivityInfo(activityVo);
        return results.ok();
    }

    //该功能未测试
    @ApiOperation("删除活动信息，同时删除活动下的所有分组和分组下的所有组员，视图只能查找和对结果进行过滤，不能对结果进行增删改，所以只能手动循环删除")
    @DeleteMapping("deleteActivity/{activityId}")
    public results deleteActivity(@PathVariable Integer activityId) {
        activityService.deleteActivity(activityId);
        return results.ok();
    }
}

