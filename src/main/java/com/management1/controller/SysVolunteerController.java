package com.management1.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.management1.commonutils.results;
import com.management1.entity.SysGroup;
import com.management1.entity.SysVolunteer;
import com.management1.entity.query.VolunteerQuery;
import com.management1.service.SysVolunteerGroupService;
import com.management1.service.SysVolunteerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@RestController
@RequestMapping("/volunteer")
@CrossOrigin
public class SysVolunteerController {
    @Resource
    private SysVolunteerService vService;

    @Resource
    private SysVolunteerGroupService vGService;

    @ApiOperation("查询所有志愿者信息")
    @GetMapping("/getAllVolunteer")
    public results getAllVolunteer() {
        List<SysVolunteer> list = vService.list(null);
        return results.ok().data("lists", list);
    }

    //根据ID值获取用户信息
    @ApiOperation("根据ID值获取用户信息")
    @GetMapping("/getVolunteerById/{id}")
    public results getVolunteerById(@PathVariable Integer id) {
        SysVolunteer volunteer = vService.getById(id);
        return results.ok().data("volunteer", volunteer);
    }

    //根据ID删除
    @ApiOperation("根据用户ID删除用户信息，在志愿者表删除后，还需要在sys_volunteer_group表中删除关联数据")
    @DeleteMapping("{id}")
    public results removeVolunteerById(@PathVariable String id) {
        boolean b1 = vService.removeById(Integer.parseInt(id));
        boolean b2 = vGService.removeById(Integer.parseInt(id));
        if (b1 && b2)
            return results.ok();
        else
            return results.error();
    }

    //@RequestBody必须用post才能接收到，required = false表示值可以为空
    @ApiOperation("组合条件查询用户信息")
    @PostMapping("/pageVolunteerCondition/{current}/{limit}")
    public results pageVolunteerCondition(@PathVariable long current, @PathVariable long limit,
                                          @RequestBody(required = false) VolunteerQuery volunteerQuery) {
        Page<SysVolunteer> pageCondition = new Page<>(current, limit);
        QueryWrapper<SysVolunteer> wrapper = new QueryWrapper<>();
        String userName = volunteerQuery.getUserName();
        String stuId = volunteerQuery.getStuId();
        String status = volunteerQuery.getStatus();
        String begin = volunteerQuery.getCreateTime();
        String end = volunteerQuery.getUpdateTime();
        if (!StringUtils.isEmpty(stuId))
            wrapper.like("stuId", stuId);
        if (!StringUtils.isEmpty(userName))
            wrapper.like("username", userName);
        if (!StringUtils.isEmpty(status))
            wrapper.eq("status", status);
        if (!StringUtils.isEmpty(begin))
            wrapper.ge("createTime", begin);
        if (!StringUtils.isEmpty(end))
            wrapper.le("updateTime", end);
        vService.page(pageCondition, wrapper);
        long total = pageCondition.getTotal();
        List<SysVolunteer> records = pageCondition.getRecords();
        return results.ok().data("total", total).data("rows", records);
    }

    @ApiOperation("增加志愿者用户")
    @PostMapping("/addVolunteer")
    public results addVolunteer(@RequestBody SysVolunteer volunteer) {
        volunteer.setAuthCode(volunteer.getStuId()+"-"+volunteer.getUsername());
        boolean b = vService.save(volunteer);
        if (b)
            return results.ok();
        else
            return results.error();
    }

    @ApiOperation("修改用户信息")
    @PostMapping("/updateVolunteer")
    public results updateVolunteer(@RequestBody SysVolunteer volunteer) {
        volunteer.setAuthCode(volunteer.getStuId()+"-"+volunteer.getUsername());
        boolean b = vService.updateById(volunteer);
        if (b)
            return results.ok();
        else
            return results.error();
    }

    @ApiOperation("查询没有分配被小组的人员，只查姓名和id值")
    @GetMapping("getMemberNotInGroup")
    public results getMemberNotInGroup() {
        List<SysVolunteer> idleList = vService.getMemberNotInGroup();
        return results.ok().data("idleList", idleList);
    }

    @ApiOperation("自动设置学号+姓名字段信息")
    @GetMapping("autoSetAuthCode")
    public results autoSetAuthCode(){
        List<SysVolunteer> volunteerList = vService.list();
        volunteerList.forEach(item -> {
            String stuId = item.getStuId();
            String username = item.getUsername();
            item.setAuthCode(stuId + "-" + username);
            vService.saveOrUpdate(item);
        });
        return results.ok();
    }
}

