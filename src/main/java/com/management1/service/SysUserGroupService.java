package com.management1.service;

import com.management1.entity.SysUserGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-26
 */
public interface SysUserGroupService extends IService<SysUserGroup> {

}
