package com.management1.service;

import com.management1.commonutils.results;
import com.management1.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysRoleService extends IService<SysRole> {

    Boolean changeRoleById(int roleID);

    //根据用户获取角色数据
    Map<String, Object> findRoleByUserId(Integer userId);

    //根据用户分配角色
    void saveUserRoleRelationShip(Integer userId, Integer[] roleId);

    List<SysRole> selectRoleByUserId(Integer id);
}
