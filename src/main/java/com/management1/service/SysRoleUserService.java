package com.management1.service;

import com.management1.entity.SysRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysRoleUserService extends IService<SysRoleUser> {

}
