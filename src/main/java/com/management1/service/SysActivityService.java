package com.management1.service;

import com.management1.entity.SysActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.management1.entity.dto.activityDto;
import com.management1.entity.vo.activityVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
public interface SysActivityService extends IService<SysActivity> {

    void saveActivityInfo(activityVo activityVo);

    List<activityVo> getActivityByPage(long current, long limit, activityVo activityVo);

    void deleteActivity(Integer activityId);
}
