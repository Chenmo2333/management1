package com.management1.service;

import com.management1.entity.SysGroup;
import com.management1.entity.SysVolunteer;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
public interface SysVolunteerService extends IService<SysVolunteer> {

    List<SysVolunteer> getMemberNotInGroup();

}
