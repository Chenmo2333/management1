package com.management1.service;

import com.management1.entity.SysVolunteer;
import com.management1.entity.SysVolunteerGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.management1.entity.activity.FirstActivity;
import com.management1.entity.dto.roleDto;
import com.management1.entity.vo.finalGroupInfo;
import com.management1.entity.vo.groupInfoVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
public interface SysVolunteerGroupService extends IService<SysVolunteerGroup> {

    //获取分组信息，最终结果中的成员和志愿者以逗号分隔开
    List<finalGroupInfo> getGroupInfoView(Integer activityId);

    //获取分组信息，最终结果中的成员和志愿者以集合形式展现
    List<groupInfoVo> getGroupInfo ();

    List<FirstActivity> getActivityGroupInfo();

    //查询角色id和角色名
    List<roleDto> getRoleList();

    //添加组员到小组（单个添加）
    boolean addMemberIntoGroup(SysVolunteerGroup volunteerGroup);

    //根据组id，查询小组内的人员，只查姓名和id值，用于分配任务
    List<SysVolunteer> getMemberByGroupId(Integer groupId);
}
