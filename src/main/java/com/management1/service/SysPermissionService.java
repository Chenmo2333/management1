package com.management1.service;

import com.alibaba.fastjson.JSONObject;
import com.management1.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysPermissionService extends IService<SysPermission> {
    //获取全部菜单
    List<SysPermission> queryAllMenu();

    //根据角色获取菜单
    List<SysPermission> selectAllMenu(Integer roleId);

    //给角色分配权限
    void saveRolePermissionRelationShip(Integer roleId, Integer[] permissionId);

    //递归删除菜单
    void removeChildById(Integer id);

    //根据用户id获取用户菜单
    List<String> selectPermissionValueByUserId(Integer id);

    List<JSONObject> selectPermissionByUserId(Integer id);
}
