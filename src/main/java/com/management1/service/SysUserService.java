package com.management1.service;

import com.alibaba.fastjson.JSONObject;
import com.management1.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysUserService extends IService<SysUser> {

    // 从数据库中取出用户信息
//    SysUser selectByUsername(String username);

    Boolean saveByUser(SysUser user);

    Boolean deleteByUserId(int parseInt);

    String login(SysUser user);

    Map<String, Object> getUserInfoByToken(Integer memberId);

    /**
     * 根据用户id获取动态菜单
     * @param memberId
     * @return List<JSONObject>
     */
    List<JSONObject> getMenu(Integer memberId);
}
