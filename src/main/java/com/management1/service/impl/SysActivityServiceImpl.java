package com.management1.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.management1.entity.*;
import com.management1.entity.dto.activityDto;
import com.management1.entity.vo.activityVo;
import com.management1.mapper.*;
import com.management1.service.SysActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.management1.service.SysVolunteerGroupService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@Service
public class SysActivityServiceImpl extends ServiceImpl<SysActivityMapper, SysActivity> implements SysActivityService {

    @Resource
    private SysActivityMapper activityMapper;

    @Resource
    private SysGroupMapper groupMapper;

    @Resource
    private SysRoleUserMapper roleUserMapper;

    @Resource
    private SysVolunteerGroupMapper volunteerGroupMapper;

    @Override
    public void saveActivityInfo(activityVo activityVo) {
        //1.向活动信息表中添加数据
        SysActivity activity = new SysActivity();
        BeanUtils.copyProperties(activityVo,activity);
        baseMapper.insert(activity);
        //2.在用户角色中间表中赋予用户活动负责人的角色
        SysRoleUser roleUser = new SysRoleUser();
        Integer userId = activityVo.getUserId();
        roleUser.setUserId(userId);
        //角色id “4” 代表活动负责人
        roleUser.setRoleId(4);
        roleUserMapper.insert(roleUser);
    }

    @Override
    public List<activityVo> getActivityByPage(long current, long limit, activityVo activityVo) {
        Page<activityVo> pageCondition = new Page<>(current, limit);
        QueryWrapper<activityVo> wrapper = new QueryWrapper<>();
        String activityName = activityVo.getName();
        String userName = activityVo.getUserName();
        Date beginTime = activityVo.getBeginTime();
        Date endTime = activityVo.getEndTime();
        if (!StringUtils.isEmpty(userName))
            wrapper.like("username", userName);
        if (!StringUtils.isEmpty(activityName))
            wrapper.like("name", activityName);
        if (!StringUtils.isEmpty(beginTime))
            wrapper.ge("createTime", beginTime);
        if (!StringUtils.isEmpty(endTime))
            wrapper.le("endTime", endTime);
        List<activityVo> list = activityMapper.getActivityByPage(pageCondition, wrapper);
        return list;
    }

    @Override
    public void deleteActivity(Integer activityId) {
        // 1.先删sys_activity表
        baseMapper.deleteById(activityId);

        // 2.再删sys_group表
        QueryWrapper<SysGroup> groupWrapper = new QueryWrapper<>();
        groupWrapper.select("id").eq("activityId",activityId);
        // 2.1查活动下对应的分组id
        List<SysGroup> sysGroups = groupMapper.selectList(groupWrapper);
        // 2.2将分组id以List集合的形式存储
        List<Integer> groupIdList = new ArrayList<>();
        for (SysGroup sysGroup : sysGroups) {
            groupIdList.add(sysGroup.getGroupId());
        }
        // 2.3最后删除活动下的分组信息
        groupMapper.delete(groupWrapper);

        // 3.再删sys_volunteer_group表
        // 根据groupId循环删除sys_volunteer_group表中的数据
        for (Integer groupIdTemp : groupIdList) {
            QueryWrapper<SysVolunteerGroup> vgWrapper = new QueryWrapper<>();
            vgWrapper.eq("groupId",groupIdTemp);
            volunteerGroupMapper.delete(vgWrapper);
        }

    }
}
