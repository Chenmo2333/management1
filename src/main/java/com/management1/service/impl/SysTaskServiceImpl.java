package com.management1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.management1.entity.SysTask;
import com.management1.mapper.SysTaskMapper;
import com.management1.service.SysTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-06-21
 */
@Service
public class SysTaskServiceImpl extends ServiceImpl<SysTaskMapper, SysTask> implements SysTaskService {

    @Override
    public List<SysTask> queryAllTask() {
        //1.查询所有的任务信息，以组id降序排序，更新操作任务id
        QueryWrapper<SysTask> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("id");
        List<SysTask> taskList = baseMapper.selectList(wrapper);
        return buildTaskList(taskList);
    }

    //构建树形任务列表，第一步：找出符合条件的所有一级任务
    public List<SysTask> buildTaskList(List<SysTask> taskList) {
        //定义一级任务初始id
        double i = 1;
        //创建list集合，用于数据最终封装
        List<SysTask> finalTaskList = new ArrayList<>();
        for (SysTask taskTemp : taskList) {
            //得到顶层菜单，pid=0
            if (taskTemp.getPid() == 0) {
                taskTemp.setLevel(1);
                finalTaskList.add(selectChildren(taskTemp, taskList, i, 1));
                taskTemp.setTaskId(i++);
                baseMapper.updateById(taskTemp);
            }
        }
        return finalTaskList;
    }

    //构建树形任务列表，第二步：递归找出一级任务下的子任务，并对其编号
    public SysTask selectChildren(SysTask task, List<SysTask> taskList, double preTaskId, double depth) {
        int i = 1;
        depth /= 10;
        //1. 因为向一层任务里面放二层任务，二层里面还要放三层，所以把对象初始化
        task.setChildren(new ArrayList<>());
        //2. 遍历所有菜单List集合，进行判断比较，比较id和pid值是否相同
        for (SysTask taskTemp : taskList) {
            //判断 id和pid值是否相同
            if (task.getId().equals(taskTemp.getPid())) {
                int level = task.getLevel() + 1;
                taskTemp.setLevel(level);
                taskTemp.setTaskId(preTaskId + depth * i);
                i++;
                //初始化子节点
                if (task.getChildren() == null) {
                    task.setChildren(new ArrayList<>());
                }
                baseMapper.updateById(taskTemp);
                task.getChildren().add(selectChildren(taskTemp, taskList, taskTemp.getTaskId(), depth));
            }
        }
        return task;
    }

    @Override
    public List<SysTask> queryAllTask1() {
        //1.查询所有的任务信息，以组id降序排序
        QueryWrapper<SysTask> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("id");
        List<SysTask> taskList = baseMapper.selectList(wrapper);
        return buildTaskList1(taskList);
    }

    //构建树形任务列表，不对任务id进行操作
    public List<SysTask> buildTaskList1(List<SysTask> taskList) {
        //定义一级任务初始id
        double i = 1;
        //创建list集合，用于数据最终封装
        List<SysTask> finalTaskList = new ArrayList<>();
        for (SysTask taskTemp : taskList) {
            //得到顶层菜单，pid=0
            if (taskTemp.getPid() == 0) {
                taskTemp.setLevel(1);
                finalTaskList.add(selectChildren1(taskTemp, taskList, i, 1));
                taskTemp.setTaskId(i++);
            }
        }
        return finalTaskList;
    }

    //构建树形任务列表，不对任务id进行操作
    public SysTask selectChildren1(SysTask task, List<SysTask> taskList, double preTaskId, double depth) {
        int i = 1;
        depth /= 10;
        //1. 因为向一层任务里面放二层任务，二层里面还要放三层，所以把对象初始化
        task.setChildren(new ArrayList<>());
        //2. 遍历所有菜单List集合，进行判断比较，比较id和pid值是否相同
        for (SysTask taskTemp : taskList) {
            //判断 id和pid值是否相同
            if (task.getId().equals(taskTemp.getPid())) {
                int level = task.getLevel() + 1;
                taskTemp.setLevel(level);
                taskTemp.setTaskId(preTaskId + depth * i);
                i++;
                //初始化子节点
                if (task.getChildren() == null) {
                    task.setChildren(new ArrayList<>());
                }
                task.getChildren().add(selectChildren(taskTemp, taskList, taskTemp.getTaskId(), depth));
            }
        }
        return task;
    }



    //递归删除任务
    @Override
    public void deleteTaskById(Integer taskId) {
        //1.创建list集合，封装需要删除的任务id
        List<Integer> idList = new ArrayList<>();
        //2.向集合中加入需要删除的任务id
        this.selectChildTaskById(taskId, idList);
        //3.记得删除传入的任务
        idList.add(taskId);
        baseMapper.deleteBatchIds(idList);
    }

    //查找所有的任务名称（不重复）
    @Override
    public List<SysTask> getAllTaskName() {
        return baseMapper.getAllTaskName();
    }


    //根据传入的任务id，查找子任务
    private void selectChildTaskById(Integer taskId, List<Integer> idList) {
        QueryWrapper<SysTask> wrapper = new QueryWrapper<>();
        wrapper.eq("pid", taskId);
        wrapper.select("id");
        List<SysTask> childIdList = baseMapper.selectList(wrapper);
        childIdList.forEach(item -> {
            //封装idList里面
            idList.add(item.getId());
            //递归查询
            this.selectChildTaskById(item.getId(), idList);
        });
    }

    //根据id，查询子任务的上一级任务
    @Override
    public SysTask getParentTaskName(Integer id) {
        SysTask childTask = baseMapper.selectById(id);
        Integer parentTaskId = childTask.getPid();
        if (parentTaskId == 0){
            //一级任务则返回自身
            return childTask;
        }else {
            //不是一级任务则找出父任务
            QueryWrapper<SysTask> wrapper = new QueryWrapper<>();
            wrapper.eq("id",parentTaskId);
            return baseMapper.selectOne(wrapper);
        }

    }
}
