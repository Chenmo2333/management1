package com.management1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.management1.entity.SysActivity;
import com.management1.entity.SysGroup;
import com.management1.entity.SysVolunteer;
import com.management1.entity.SysVolunteerGroup;
import com.management1.entity.activity.FirstActivity;
import com.management1.entity.activity.TwoGroup;
import com.management1.entity.dto.groupInfoViewDto;
import com.management1.entity.dto.roleDto;
import com.management1.entity.vo.finalGroupInfo;
import com.management1.entity.vo.groupInfoVo;
import com.management1.entity.vo.groupMemberVo;
import com.management1.mapper.SysActivityMapper;
import com.management1.mapper.SysGroupMapper;
import com.management1.mapper.SysVolunteerGroupMapper;
import com.management1.service.SysVolunteerGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@Service
public class SysVolunteerGroupServiceImpl extends ServiceImpl<SysVolunteerGroupMapper, SysVolunteerGroup> implements SysVolunteerGroupService {
    @Resource
    private SysGroupMapper groupMapper;

    @Resource
    private SysActivityMapper activityMapper;

    @Override
    public List<finalGroupInfo> getGroupInfoView(Integer activityId) {
        //获取总的分组信息
        List<groupInfoViewDto> groupInfoView = baseMapper.getGroupInfoView(activityId);
        //对信息进行分类
        Map<Integer, List<groupMemberVo>> volunteerMap = new HashMap<>();
        Map<Integer, List<groupMemberVo>> memberMap = new HashMap<>();
        Map<Integer, groupMemberVo> leadMap = new HashMap<>();
        Map<Integer, groupMemberVo> leadTwoMap = new HashMap<>();

        for (groupInfoViewDto tag : groupInfoView) {
            //定义中间变量存储各个对象
            groupMemberVo volunteerTemp = new groupMemberVo();
            groupMemberVo memberTemp = new groupMemberVo();
            groupMemberVo leaderTemp = new groupMemberVo();
            groupMemberVo leaderTwoTemp = new groupMemberVo();
            //如果该分组不存在则加入map集合的key中,如果不提前加入，则会报空指针异常
            volunteerMap.computeIfAbsent(tag.getGroupId(), k -> new ArrayList<>());
            memberMap.computeIfAbsent(tag.getGroupId(), k -> new ArrayList<>());
            //将志愿者加入对应的分组中
            if ("志愿者".equals(tag.getRoleName())) {
                volunteerTemp.setRoleName("志愿者");
                volunteerTemp.setGroupId(tag.getGroupId());
                volunteerTemp.setUserId(tag.getVolunteerId());
                volunteerTemp.setUserName(tag.getVolunteerName());
                volunteerMap.get(tag.getGroupId()).add(volunteerTemp);
            } else if ("组员".equals(tag.getRoleName())) {
                memberTemp.setRoleName("组员");
                memberTemp.setGroupId(tag.getGroupId());
                memberTemp.setUserId(tag.getVolunteerId());
                memberTemp.setUserName(tag.getVolunteerName());
                memberMap.get(tag.getGroupId()).add(memberTemp);
            } else if ("组长".equals(tag.getRoleName())) {
                leaderTemp.setGroupId(tag.getGroupId());
                leaderTemp.setRoleName("组长");
                leaderTemp.setUserId(tag.getVolunteerId());
                leaderTemp.setUserName(tag.getVolunteerName());
                leadMap.put(tag.getGroupId(), leaderTemp);
            } else if ("副组长".equals(tag.getRoleName())) {
                //将副组长加入对应分组中
                leaderTwoTemp.setGroupId(tag.getGroupId());
                leaderTwoTemp.setRoleName("副组长");
                leaderTwoTemp.setUserId(tag.getVolunteerId());
                leaderTwoTemp.setUserName(tag.getVolunteerName());
                leadTwoMap.put(tag.getGroupId(), leaderTwoTemp);
            }
        }

        //定义最终需要返回的结果集
        List<groupInfoVo> groupInfos = new ArrayList<>();
        //查询分组信息
        QueryWrapper<SysGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("activityId", activityId);
        wrapper.orderByAsc("groupId");
        List<SysGroup> sysGroups = groupMapper.selectList(wrapper);
        //封装进集合
        for (SysGroup sysGroup : sysGroups) {
            groupInfoVo info = new groupInfoVo();
            List<groupMemberVo> sysVolunteers = volunteerMap.get(sysGroup.getGroupId());
            List<groupMemberVo> groupMemberVos = memberMap.get(sysGroup.getGroupId());

            info.setLeader(leadMap.get(sysGroup.getGroupId()));
            info.setLeaderTwo(leadTwoMap.get(sysGroup.getGroupId()));
            info.setActivityId(sysGroup.getGroupId());
            info.setGroupName(sysGroup.getGroupName());
            info.setDescription(sysGroup.getDescription());
            info.setVolunteers(sysVolunteers);
            info.setMembers(groupMemberVos);

            groupInfos.add(info);
        }
        List<finalGroupInfo> finalGroupInfoList = new ArrayList<>();
        //将志愿者和普通组员封装，以逗号隔开
        for (groupInfoVo groupInfo : groupInfos) {
            List<groupMemberVo> members = groupInfo.getMembers();
            List<groupMemberVo> volunteers = groupInfo.getVolunteers();
            List<String> membersNameList = new ArrayList<>();
            List<String> volunteersNameList = new ArrayList<>();
            String membersName = "";
            String volunteersName = "";
            if (null != members && members.size() > 0)
                for (groupMemberVo member : members) {
                    membersNameList.add(member.getUserName());
                }

            if (null != volunteers && volunteers.size() > 0)
                for (groupMemberVo volunteer : volunteers) {
                    volunteersNameList.add(volunteer.getUserName());
                }

            if (membersNameList.size() > 0)
                membersName = StringUtils.join(membersNameList, ",");

            if (volunteersNameList.size() > 0)
                volunteersName = StringUtils.join(volunteersNameList, ",");

            finalGroupInfo finalGroupInfo = new finalGroupInfo();
            finalGroupInfo.setId(groupInfo.getActivityId());
            finalGroupInfo.setGroupName(groupInfo.getGroupName());
            finalGroupInfo.setDescription(groupInfo.getDescription());
            finalGroupInfo.setVolunteers(volunteersName);
            finalGroupInfo.setMembers(membersName);
            String leaderName = "";
            String leaderTwoName = "";
            if (groupInfo.getLeader() != null) {
                leaderName = groupInfo.getLeader().getUserName();
            }
            if (groupInfo.getLeaderTwo() != null) {
                leaderTwoName = groupInfo.getLeaderTwo().getUserName();
            }
            finalGroupInfo.setLeader(leaderName);
            finalGroupInfo.setLeaderTwo(leaderTwoName);
            finalGroupInfoList.add(finalGroupInfo);
        }
        return finalGroupInfoList;
    }

    public List<groupInfoVo> getGroupInfo() {
        //获取总的分组信息
        List<groupInfoViewDto> groupInfoView = baseMapper.getGroupInfo();
        //对信息进行分类
        Map<Integer, List<groupMemberVo>> volunteerMap = new HashMap<>();
        Map<Integer, List<groupMemberVo>> memberMap = new HashMap<>();
        Map<Integer, groupMemberVo> leadMap = new HashMap<>();
        Map<Integer, groupMemberVo> leadTwoMap = new HashMap<>();

        for (groupInfoViewDto tag : groupInfoView) {
            //定义中间变量存储各个对象
            groupMemberVo volunteerTemp = new groupMemberVo();
            groupMemberVo memberTemp = new groupMemberVo();
            groupMemberVo leaderTemp = new groupMemberVo();
            groupMemberVo leaderTwoTemp = new groupMemberVo();
            //如果该分组不存在则加入map集合的key中,如果不提前加入，则会报空指针异常
            volunteerMap.computeIfAbsent(tag.getGroupId(), k -> new ArrayList<>());
            memberMap.computeIfAbsent(tag.getGroupId(), k -> new ArrayList<>());
            //将志愿者加入对应的分组中
            if ("志愿者".equals(tag.getRoleName())) {
                volunteerTemp.setRoleName("志愿者");
                volunteerTemp.setGroupId(tag.getGroupId());
                volunteerTemp.setUserId(tag.getVolunteerId());
                volunteerTemp.setUserName(tag.getVolunteerName());
                volunteerMap.get(tag.getGroupId()).add(volunteerTemp);
            } else if ("组员".equals(tag.getRoleName())) {
                memberTemp.setRoleName("组员");
                memberTemp.setGroupId(tag.getGroupId());
                memberTemp.setUserId(tag.getVolunteerId());
                memberTemp.setUserName(tag.getVolunteerName());
                memberMap.get(tag.getGroupId()).add(memberTemp);
            } else if ("组长".equals(tag.getRoleName())) {
                leaderTemp.setGroupId(tag.getGroupId());
                leaderTemp.setRoleName("组长");
                leaderTemp.setUserId(tag.getVolunteerId());
                leaderTemp.setUserName(tag.getVolunteerName());
                leadMap.put(tag.getGroupId(), leaderTemp);
            } else if ("副组长".equals(tag.getRoleName())) {
                //将副组长加入对应分组中
                leaderTwoTemp.setGroupId(tag.getGroupId());
                leaderTwoTemp.setRoleName("副组长");
                leaderTwoTemp.setUserId(tag.getVolunteerId());
                leaderTwoTemp.setUserName(tag.getVolunteerName());
                leadTwoMap.put(tag.getGroupId(), leaderTwoTemp);
            }
        }

        //定义最终需要返回的结果集
        List<groupInfoVo> groupInfos = new ArrayList<>();
        //查询所有分组信息
        List<SysGroup> sysGroups = groupMapper.selectList(null);
        //封装进集合
        for (SysGroup sysGroup : sysGroups) {
            groupInfoVo info = new groupInfoVo();
            List<groupMemberVo> sysVolunteers = volunteerMap.get(sysGroup.getGroupId());
            List<groupMemberVo> groupMemberVos = memberMap.get(sysGroup.getGroupId());

            info.setLeader(leadMap.get(sysGroup.getGroupId()));
            info.setLeaderTwo(leadTwoMap.get(sysGroup.getGroupId()));
            info.setActivityId(sysGroup.getGroupId());
            info.setGroupName(sysGroup.getGroupName());
            info.setDescription(sysGroup.getDescription());
            info.setVolunteers(sysVolunteers);
            info.setMembers(groupMemberVos);

            groupInfos.add(info);
        }
        return groupInfos;
    }

    @Override
    public List<FirstActivity> getActivityGroupInfo() {
        //先查出所有活动
        List<SysActivity> activityList = activityMapper.selectList(null);
        //再查出所有分组
        List<SysGroup> groupList = groupMapper.selectList(null);
        List<FirstActivity> list = new ArrayList<>();
        for (SysActivity activity : activityList) {
            FirstActivity firstActivity = new FirstActivity();
            firstActivity.setActivityId(activity.getId());
            firstActivity.setActivityName(activity.getName());
            List<TwoGroup> twoGroupList = new ArrayList<>();
            for (SysGroup group : groupList) {
                if (activity.getId().equals(group.getActivityId())) {
                    TwoGroup twoGroup = new TwoGroup();
                    twoGroup.setGroupId(group.getGroupId());
                    twoGroup.setGroupName(group.getGroupName());
                    twoGroupList.add(twoGroup);
                }
            }
            firstActivity.setGroups(twoGroupList);
            list.add(firstActivity);
        }
        return list;
    }

    @Override
    public List<roleDto> getRoleList() {
        return baseMapper.getRoleList();
    }

    /**
     * volunteerGroup中包含的信息
     * stuId: "S125432749"
     * roleId: 5
     * groupId: 6
     * activityId: 1
     * volunteerId: 23
     * roleName: ""
     * groupName: "食宿医保障"
     * activityName: "中国软件杯软件设计大赛"
     * volunteerName: ""
     * roleDescription: "噢噢噢噢"
     * 所以需要手动为roleName赋值
     */
    @Override
    public boolean addMemberIntoGroup(SysVolunteerGroup volunteerGroup) {
        SysVolunteerGroup temp = new SysVolunteerGroup();
        Integer roleId = volunteerGroup.getRoleId();
        String roleName = baseMapper.getRoleName(roleId);
        BeanUtils.copyProperties(volunteerGroup, temp);
        temp.setRoleName(roleName);

        int insert = baseMapper.insert(temp);
        return insert > 0;
    }

    //根据组id，查询小组内的人员，只查姓名和id值，用于分配任务
    @Override
    public List<SysVolunteer> getMemberByGroupId(Integer groupId) {
        return baseMapper.getMemberByGroupId(groupId);
    }
}
