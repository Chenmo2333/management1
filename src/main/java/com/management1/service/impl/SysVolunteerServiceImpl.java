package com.management1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.management1.entity.SysGroup;
import com.management1.entity.SysVolunteer;
import com.management1.entity.SysVolunteerGroup;
import com.management1.mapper.SysVolunteerGroupMapper;
import com.management1.mapper.SysVolunteerMapper;
import com.management1.service.SysVolunteerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@Service
public class SysVolunteerServiceImpl extends ServiceImpl<SysVolunteerMapper, SysVolunteer> implements SysVolunteerService {

    @Override
    public List<SysVolunteer> getMemberNotInGroup() {
        return baseMapper.getMemberNotInGroup();
    }

}
