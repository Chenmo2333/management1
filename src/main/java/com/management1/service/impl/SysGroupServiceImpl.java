package com.management1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.management1.entity.SysGroup;
import com.management1.entity.SysUser;
import com.management1.entity.SysVolunteer;
import com.management1.entity.dto.groupInfoDto;
import com.management1.entity.vo.*;
import com.management1.mapper.SysGroupMapper;
import com.management1.service.SysGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.runtime.directive.Foreach;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@Service
public class SysGroupServiceImpl extends ServiceImpl<SysGroupMapper, SysGroup> implements SysGroupService {

}
