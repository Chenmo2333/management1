package com.management1.service.impl;

import com.management1.entity.SysRoleUser;
import com.management1.mapper.SysRoleUserMapper;
import com.management1.service.SysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements SysRoleUserService {

}
