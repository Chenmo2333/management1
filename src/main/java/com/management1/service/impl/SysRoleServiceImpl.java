package com.management1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.management1.entity.SysRole;
import com.management1.entity.SysRoleUser;
import com.management1.mapper.SysRoleMapper;
import com.management1.mapper.SysRoleUserMapper;
import com.management1.service.SysRoleService;
import com.management1.service.SysRoleUserService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.sql.Wrapper;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Resource
    private SysRoleUserMapper roleUserMapper;

    @Resource
    private SysRoleUserService roleUserService;

    @Override
    public Boolean changeRoleById(int roleId) {
        Map<String,Object> map = new HashMap<>();
        map.put("roleId",roleId);
        List<SysRoleUser> sysRoleUsers = roleUserMapper.selectByMap(map);
//        List<SysRoleUser> sysRoleUsers = roleUserMapper.findByRoleId(roleId);
        if (!sysRoleUsers.isEmpty()) {
            roleUserMapper.changeRoleId(roleId);
        }
        return true;
    }

    //根据用户获取角色数据
    @Override
    public Map<String, Object> findRoleByUserId(Integer userId) {
        //查询所有的角色
        List<SysRole> allRolesList =baseMapper.selectList(null);

        //根据用户id，查询用户拥有的角色id
        List<SysRoleUser> existUserRoleList = roleUserService.list(new QueryWrapper<SysRoleUser>().eq("userId", userId).select("roleId"));

        List<Integer> existRoleList = existUserRoleList.stream().map(SysRoleUser::getRoleId).collect(Collectors.toList());

        //对角色进行分类
        List<SysRole> assignRoles = new ArrayList<>();
        for (SysRole role : allRolesList) {
            //已分配
            if(existRoleList.contains(role.getId())) {
                assignRoles.add(role);
            }
        }

        Map<String, Object> roleMap = new HashMap<>();
        roleMap.put("assignRoles", assignRoles);
        roleMap.put("allRolesList", allRolesList);
        return roleMap;
    }

    //根据用户分配角色
    @Override
    public void saveUserRoleRelationShip(Integer userId, Integer[] roleIds) {
        roleUserService.remove(new QueryWrapper<SysRoleUser>().eq("userId", userId));

        List<SysRoleUser> userRoleList = new ArrayList<>();
        for(Integer roleId : roleIds) {
            if(StringUtils.isEmpty(roleId)) continue;
            SysRoleUser userRole = new SysRoleUser();
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);

            userRoleList.add(userRole);
        }
        roleUserService.saveBatch(userRoleList);
    }

    //根据用户id查询角色信息
    @Override
    public List<SysRole> selectRoleByUserId(Integer id) {
        //根据用户id拥有的角色id
        List<SysRoleUser> userRoleList = roleUserService.list(new QueryWrapper<SysRoleUser>().eq("userId", id).select("roleId"));
        List<Integer> roleIdList = userRoleList.stream().map(SysRoleUser::getRoleId).collect(Collectors.toList());
        List<SysRole> roleList = new ArrayList<>();
        if(roleIdList.size() > 0) {
            roleList = baseMapper.selectBatchIds(roleIdList);
        }
        return roleList;
    }
}
