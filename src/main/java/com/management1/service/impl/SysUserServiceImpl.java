package com.management1.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.management1.commonutils.JwtUtils;
import com.management1.entity.SysRole;
import com.management1.entity.SysRoleUser;
import com.management1.entity.SysUser;
import com.management1.handler.MyException;
import com.management1.mapper.SysRoleMapper;
import com.management1.mapper.SysRoleUserMapper;
import com.management1.mapper.SysUserMapper;
import com.management1.service.SysPermissionService;
import com.management1.service.SysRoleService;
import com.management1.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Resource
    private SysRoleUserMapper roleUserMapper;

    @Resource
    private SysRoleService roleService;

    @Resource
    private SysPermissionService permissionService;

    @Override
    public Boolean saveByUser(SysUser user) {
        if (user != null) {
            SysRoleUser roleUser = new SysRoleUser();
            roleUser.setUserId(user.getId());
            roleUser.setRoleId(2);
            roleUserMapper.insert(roleUser);
            return true;
        } else
            return false;

    }

    @Override
    public Boolean deleteByUserId(int userID) {
        return roleUserMapper.deleteByUserID(userID);
    }

    @Override
    public String login(SysUser user) {
        String username = user.getUsername();
        String password = user.getPassword();
        if (StringUtils.isEmpty(username)){
            throw new MyException(20001,"用户名不能为空");
        }
        if (StringUtils.isEmpty(password)){
            throw new MyException(20001,"密码不能为空");
        }
        SysUser sysUser = baseMapper.selectOne(new QueryWrapper<SysUser>().eq("username", username));
        if (sysUser==null) {
            throw new MyException(20001,"用户不存在");
        }
        if (!password.equals(sysUser.getPassword())) {
            throw new MyException(20001,"密码不正确");
        }
        //账号状态，1为激活，0为未激活
        if (sysUser.getStatus()) {
            throw new MyException(20001,"账号已被禁用");
        }
        //返回token
        return JwtUtils.getJwtToken(sysUser.getId(), sysUser.getNickname());
    }

    @Override
    public Map<String, Object> getUserInfoByToken(Integer memberId) {
        Map<String, Object> result = new HashMap<>();
        SysUser user = baseMapper.selectById(memberId);

        //根据用户id获取角色
        List<SysRole> roleList = roleService.selectRoleByUserId(memberId);
        List<String> roleNameList = roleList.stream().map(SysRole::getName).collect(Collectors.toList());
        if(roleNameList.size() == 0) {
            //前端框架必须返回一个角色，否则报错，如果没有角色，返回一个空角色
            roleNameList.add("");
        }

        //根据用户id获取操作权限值
        List<String> permissionValueList = permissionService.selectPermissionValueByUserId(user.getId());
        result.put("name", user.getUsername());
        result.put("avatar", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        result.put("roles", roleNameList);
        result.put("permissionValueList", permissionValueList);
        return result;
    }

    //根据用户id获取动态菜单
    @Override
    public List<JSONObject> getMenu(Integer memberId) {
        //根据用户id获取用户菜单权限
        return permissionService.selectPermissionByUserId(memberId);
    }
}
