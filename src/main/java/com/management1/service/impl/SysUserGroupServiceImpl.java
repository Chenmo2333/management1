package com.management1.service.impl;

import com.management1.entity.SysUserGroup;
import com.management1.mapper.SysUserGroupMapper;
import com.management1.service.SysUserGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-26
 */
@Service
public class SysUserGroupServiceImpl extends ServiceImpl<SysUserGroupMapper, SysUserGroup> implements SysUserGroupService {

}
