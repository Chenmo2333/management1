package com.management1.service.impl;

import com.management1.entity.SysRolePermission;
import com.management1.mapper.SysRolePermissionMapper;
import com.management1.service.SysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements SysRolePermissionService {

}
