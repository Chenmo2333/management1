package com.management1.service;

import com.management1.entity.SysGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.management1.entity.SysUser;
import com.management1.entity.SysVolunteer;
import com.management1.entity.dto.groupInfoDto;
import com.management1.entity.vo.finalGroupInfo;
import com.management1.entity.vo.groupInfoVo;
import com.management1.entity.vo.groupVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
public interface SysGroupService extends IService<SysGroup> {

}
