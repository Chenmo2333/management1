package com.management1.service;

import com.management1.entity.SysTask;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmo
 * @since 2021-06-21
 */
public interface SysTaskService extends IService<SysTask> {

    List<SysTask> queryAllTask();

    List<SysTask> queryAllTask1();

    List<SysTask> buildTaskList(List<SysTask> taskList);

    List<SysTask> buildTaskList1(List<SysTask> taskList);

    void deleteTaskById(Integer taskId);

    List<SysTask> getAllTaskName();

    SysTask getParentTaskName(Integer id);
}
