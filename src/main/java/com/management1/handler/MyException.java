package com.management1.handler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor //生成有参构造
@NoArgsConstructor  //生成无参构造
public class MyException extends RuntimeException{
    //状态码
    private Integer code;
    //异常信息
    private String msg;
}
