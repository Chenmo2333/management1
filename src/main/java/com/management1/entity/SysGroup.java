package com.management1.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysGroup对象", description="")
public class SysGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer groupId;

    @ApiModelProperty(value = "组号对应的活动id")
    @TableField("activityId")
    private Integer activityId;

    @ApiModelProperty(value = "组名")
    @TableField("groupName")
    private String groupName;

    @ApiModelProperty(value = "组任务描述")
    private String description;

    @TableLogic
    @TableField(value = "isDeleted",fill = FieldFill.INSERT)
    private Integer isDeleted;

    @TableField(value = "createTime",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2020-01-01 10:10:10")
    private Date createTime;

    @TableField(value = "updateTime",fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更改时间", example = "2020-02-01 10:10:10")
    private Date updateTime;


}
