package com.management1.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class groupMemberVo implements Serializable {


    @ApiModelProperty(value = "组id")
    @TableField("groupId")
    private Integer groupId;

    @ApiModelProperty(value = "组成员id")
    @TableField("userId")
    private Integer userId;

    @ApiModelProperty(value = "组成员姓名")
    @TableField("userName")
    private String userName;

    @ApiModelProperty(value = "组成员角色")
    @TableField("roleName")
    private String roleName;
}
