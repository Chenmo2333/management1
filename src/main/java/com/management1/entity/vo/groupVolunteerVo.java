package com.management1.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class groupVolunteerVo implements Serializable {

    @ApiModelProperty(value = "组id")
    @TableField("groupId")
    private Integer groupId;

    @ApiModelProperty(value = "志愿者id")
    @TableField("volunteerId")
    private Integer volunteerId;

    @ApiModelProperty(value = "志愿者姓名")
    @TableField("volunteerName")
    private String volunteerName;

}
