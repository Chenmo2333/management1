package com.management1.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class groupInfoVo implements Serializable {

    @ApiModelProperty(value = "活动id")
    private Integer activityId;

    @ApiModelProperty(value = "活动名称")
    private String activityName;

    @ApiModelProperty(value = "组名")
    private String groupName;

    @ApiModelProperty(value = "组描述")
    private String description;

    @ApiModelProperty(value = "志愿者集合，这个类感觉要放在dto包中")
    private List<groupMemberVo> volunteers;

    @ApiModelProperty(value = "组成员集合")
    private List<groupMemberVo> members;

    @ApiModelProperty(value = "组长")
    private groupMemberVo leader;

    @ApiModelProperty(value = "副组长")
    private groupMemberVo leaderTwo;
}
