package com.management1.entity.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.management1.entity.SysUser;
import com.management1.entity.SysVolunteer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class groupVo {
    @ApiModelProperty(value = "组ID")
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private Integer id;

    @ApiModelProperty(value = "活动id")
    @TableField("activityId")
    private Integer activityId;

    @ApiModelProperty(value = "组号")
    @TableField("groupId")
    private Integer groupId;

    @ApiModelProperty(value = "组名")
    private String name;

    @ApiModelProperty(value = "介绍")
    private String description;

    private String volunteer;

    private String user;

    @ApiModelProperty(value = "分组下对应的志愿者")
    private List<SysVolunteer> volunteers = new ArrayList<>();

    @ApiModelProperty(value = "分组下对应的用户，作为普通组员")
    private List<SysUser> users = new ArrayList<>();
}
