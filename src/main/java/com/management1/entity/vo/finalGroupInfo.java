package com.management1.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class finalGroupInfo implements Serializable {
    @ApiModelProperty(value = "组id")
    private Integer id;

    @ApiModelProperty(value = "组名")
    private String groupName;

    @ApiModelProperty(value = "组描述")
    private String description;

    @ApiModelProperty(value = "志愿者姓名集合，以逗号分隔开")
    private String volunteers;

    @ApiModelProperty(value = "组成员姓名集合，以逗号分隔开")
    private String members;

    @ApiModelProperty(value = "组长姓名")
    private String leader;

    @ApiModelProperty(value = "副组长姓名")
    private String leaderTwo;
}
