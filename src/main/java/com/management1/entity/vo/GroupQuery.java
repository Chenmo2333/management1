package com.management1.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GroupQuery {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "活动id")
    private Integer activityId;

    @ApiModelProperty(value = "活动名称")
    private String activityName;

    @ApiModelProperty(value = "用户别名,模糊查询")
    private String nickName;

    @ApiModelProperty(value = "账户状态", example = "true")
    private String status;

    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String createTime;//注意，这里使用的是String类型，前端传过来的数据无需进行类型转换

    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String updateTime;
}
