package com.management1.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.text.DateFormat;
import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysUser对象", description="")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String username;

    private String password;

    private String nickname;

    @TableField("headImgUrl")
    private String headImgUrl;

    private String phone;

    private String telephone;

    private String email;

    private String position;

    @TableField(value = "positionId")
    private String positionId;

    private String institution;

    @TableLogic
    @TableField(value = "isDeleted",fill = FieldFill.INSERT)
    private Integer isDeleted;

    @ApiModelProperty(value = "生日", example = "2019-02-01 10:10:10")
    private Date birthday;

    @ApiModelProperty(value = "用户性别", example = "1")
    private Boolean sex;

    @ApiModelProperty(value = "账户状态", example = "1")
    private Boolean status;

    @TableField(value = "createTime",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-01-01 10:10:10")
    private Date createTime;

    @TableField(value = "updateTime",fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更改时间", example = "2019-02-01 10:10:10")
    private Date updateTime;

}
