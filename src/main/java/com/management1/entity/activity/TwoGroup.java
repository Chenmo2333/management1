package com.management1.entity.activity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class TwoGroup implements Serializable {
    @ApiModelProperty(value = "组号")
    private Integer groupId;

    @ApiModelProperty(value = "组名")
    private String groupName;
}
