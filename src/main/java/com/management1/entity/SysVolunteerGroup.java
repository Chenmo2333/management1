package com.management1.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysVolunteerGroup对象", description="")
public class SysVolunteerGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "志愿者ID")
    @TableId(value = "volunteerId")
    private Integer volunteerId;

    @ApiModelProperty(value = "组ID")
    @TableField("groupId")
    private Integer groupId;

    @ApiModelProperty(value = "角色名")
    @TableField("roleName")
    private String roleName;

    @ApiModelProperty(value = "角色id，1位组长，2为副组长，3为执行组长，4为普通组员，5为志愿者")
    @TableField("roleId")
    private Integer roleId;

    @ApiModelProperty(value = "角色描述")
    @TableField("roleDescription")
    private String roleDescription;

    @TableLogic
    @TableField(value = "isDeleted",fill = FieldFill.INSERT)
    private Integer isDeleted;

    @TableField(value = "createTime",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-01-01 10:10:10")
    private Date createTime;

    @TableField(value = "updateTime",fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更改时间", example = "2019-02-01 10:10:10")
    private Date updateTime;

}
