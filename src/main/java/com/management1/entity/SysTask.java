package com.management1.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenmo
 * @since 2021-06-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysTask对象", description="对应sys_task表")
public class SysTask implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "任务id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "是否有父级任务，0表示无")
    @TableField("pid")
    private Integer pid;

    @ApiModelProperty(value = "随机生成的任务id")
    @TableField("randomId")
    private String randomId;

    @ApiModelProperty(value = "活动id")
    @TableField("activityId")
    private Integer activityId;

    @ApiModelProperty(value = "任务名称")
    @TableField("taskName")
    private String taskName;

    @ApiModelProperty(value = "任务描述")
    @TableField("taskDescription")
    private String taskDescription;

    @ApiModelProperty(value = "发起人id")
    @TableField("sponsorId")
    private Integer sponsorId;

    @ApiModelProperty(value = "发起人姓名")
    @TableField("sponsorName")
    private String sponsorName;

    @ApiModelProperty(value = "执行人id")
    @TableField("executorId")
    private Integer executorId;

    @ApiModelProperty(value = "发起人姓名")
    @TableField("executorName")
    private String executorName;

    @ApiModelProperty(value = "组别")
    @TableField("groupName")
    private String groupName;

    @ApiModelProperty(value = "组号")
    @TableField("groupId")
    private Integer groupId;

    @ApiModelProperty(value = "任务状态，未开始、进行中、已完成、暂停中、已过期")
    @TableField("taskStatus")
    private String taskStatus;

    @ApiModelProperty(value = "任务开始时间", example = "2019-01-01 10:10:10")
    @TableField(value = "beginTime")
    private Date beginTime;

    @ApiModelProperty(value = "任务结束时间", example = "2019-02-01 10:10:10")
    @TableField(value = "endTime")
    private Date endTime;

    @ApiModelProperty(value = "创建时间", example = "2019-01-01 10:10:10")
    @TableField(value = "createTime",fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更改时间", example = "2019-02-01 10:10:10")
    @TableField(value = "updateTime",fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableLogic
    @TableField(value = "isDeleted",fill = FieldFill.INSERT)
    private Integer isDeleted;

    @ApiModelProperty(value = "前端显示的任务id")
    @TableField(value = "taskId")
    private double taskId;

    @ApiModelProperty(value = "层级，1表示为一级任务")
    @TableField(exist = false)
    private Integer level;

    @ApiModelProperty(value = "下级")
    @TableField(exist = false)
    private List<SysTask> children;
}
