package com.management1.entity.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class groupInfoViewDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "活动id")
    @TableField(value = "activityId")
    private Integer activityId;

    @ApiModelProperty(value = "活动名称")
    @TableField(value = "activityName")
    private String activityName;

    @ApiModelProperty(value = "组id")
    @TableField(value = "id")
    private Integer groupId;

    @ApiModelProperty(value = "组名")
    @TableField(value = "groupName")
    private String groupName;

    @ApiModelProperty(value = "组介绍")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "志愿者id")
    @TableField(value = "volunteerId")
    private Integer volunteerId;

    @ApiModelProperty(value = "志愿者姓名")
    @TableField(value = "volunteerName")
    private String volunteerName;

    @ApiModelProperty(value = "角色id")
    @TableField(value = "roleId")
    private Integer roleId;

    @ApiModelProperty(value = "角色名")
    @TableField(value = "roleName")
    private String roleName;

    @ApiModelProperty(value = "角色描述")
    @TableField(value = "roleDescription")
    private String roleDescription;
}
