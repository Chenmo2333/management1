package com.management1.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class groupMemberDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组id")
    private Integer groupId;

    @ApiModelProperty(value = "组员id")
    private Integer userId;

    @ApiModelProperty(value = "组员姓名")
    private String userName;

    @ApiModelProperty(value = "角色名")
    private String roleName;
}
