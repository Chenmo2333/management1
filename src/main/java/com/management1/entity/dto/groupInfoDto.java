package com.management1.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class groupInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "活动名称")
    private Integer activityName;

    @ApiModelProperty(value = "组id")
    private Integer groupId;

    @ApiModelProperty(value = "组名")
    private String groupName;

    @ApiModelProperty(value = "组介绍")
    private String description;

    @ApiModelProperty(value = "志愿者id")
    private Integer volunteerId;

    @ApiModelProperty(value = "志愿者姓名")
    private String volunteer;

    @ApiModelProperty(value = "组成员id")
    private Integer userId;

    @ApiModelProperty(value = "组成员姓名")
    private String userName;

    @ApiModelProperty(value = "组成员角色名")
    private String roleName;
}
