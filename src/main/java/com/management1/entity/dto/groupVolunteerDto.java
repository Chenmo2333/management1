package com.management1.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class groupVolunteerDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组id")
    private Integer groupId;

    @ApiModelProperty(value = "志愿者id")
    private Integer volunteerId;

    @ApiModelProperty(value = "志愿者姓名")
    private String volunteerName;
}
