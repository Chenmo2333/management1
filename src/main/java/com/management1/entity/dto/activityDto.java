package com.management1.entity.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="activityVo对象,在前端显示活动基本基本信息，增加了负责人信息", description="")
public class activityDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private Integer id;

    @ApiModelProperty(value = "活动名称")
    private String name;

    @ApiModelProperty(value = "活动描述")
    private String description;

    @ApiModelProperty(value = "活动负责人id")
    private String userId;

    @ApiModelProperty(value = "活动负责人姓名")
    private String userName;

    @ApiModelProperty(value = "开始时间")
    @TableField("beginTime")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间")
    @TableField("endTime")
    private Date endTime;

    @TableField(value = "createTime",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2020-01-01 10:10:10")
    private Date createTime;

    @TableField(value = "updateTime",fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更改时间", example = "2020-02-01 10:10:10")
    private Date updateTime;
}
