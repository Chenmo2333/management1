package com.management1.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenmo
 * @since 2021-05-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysUserGroup对象", description="废弃的类，用户与组的联系改为体现在SysVolunteerGroup表里")
public class SysUserGroup implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "userId", type = IdType.ID_WORKER)
    private Integer userId;

    @ApiModelProperty(value = "分组id")
    @TableField("groupId")
    private Integer groupId;

    @TableLogic
    @TableField(value = "isDeleted",fill = FieldFill.INSERT)
    private Integer isDeleted;


}
