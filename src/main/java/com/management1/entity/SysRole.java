package com.management1.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysRole对象", description="")
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    private String description;

    @TableLogic
    @TableField(value = "isDeleted",fill = FieldFill.INSERT)
    private Integer isDeleted;

    @TableField(value = "createTime",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-01-01 10:10:10")
    private Date createTime;

    @TableField(value = "updateTime",fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更改时间", example = "2019-02-01 10:10:10")
    private Date updateTime;
}
