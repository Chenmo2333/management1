package com.management1.entity.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "Volunteer查询对象", description = "志愿者查询对象封装")
@Data
public class VolunteerQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "学号,模糊查询")
    private String stuId;

    @ApiModelProperty(value = "用户名称,模糊查询")
    private String userName;

    @ApiModelProperty(value = "账户状态", example = "true")
    private String status;

    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String createTime;

    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String updateTime;
}
