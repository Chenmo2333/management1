package com.management1.entity.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

@ApiModel(value = "user查询对象", description = "用户查询对象封装")
@Data
public class userQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名称,模糊查询")
    private String userName;

    @ApiModelProperty(value = "用户别名,模糊查询")
    private String nickName;

    @ApiModelProperty(value = "账户状态", example = "true")
    private String status;

    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String createTime;//注意，这里使用的是String类型，前端传过来的数据无需进行类型转换

    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String updateTime;
}
