package com.management1.entity.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GroupQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    //    @TableField("activityId")
    @ApiModelProperty(value = "组号对应的活动id")
    private Integer activityId;

    @ApiModelProperty(value = "活动名称")
    private String activityName;

    @ApiModelProperty(value = "组号")
    private Integer groupId;

    @ApiModelProperty(value = "组名")
    private String groupName;

}
