package com.management1.commonutils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class results {
    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String, Object> data = new HashMap<String, Object>();

    //私有化构造方法，使其不能随便new对象
    private results(){}

    public static results ok(){
        results r = new results();
        r.setSuccess(true);
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");
        return r;
    }

    public static results error(){
        results r = new results();
        r.setSuccess(false);
        r.setCode(ResultCode.ERROR);
        r.setMessage("失败");
        return r;
    }

    public static results error(String message){
        results r = new results();
        r.setSuccess(false);
        r.setCode(ResultCode.ERROR);
        r.setMessage(message);
        return r;
    }

    //下面的类是为了实现链式编程
    public results success(Boolean success){
        this.setSuccess(success);
        return this;
    }

    public results message(String message){
        this.setMessage(message);
        return this;
    }

    public results code(Integer code){
        this.setCode(code);
        return this;
    }

    public results data(String key, Object value){
        this.data.put(key, value);
        return this;
    }

    public results data(Map<String, Object> map){
        this.setData(map);
        return this;
    }
}
