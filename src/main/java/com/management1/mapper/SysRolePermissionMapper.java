package com.management1.mapper;

import com.management1.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
