package com.management1.mapper;

import com.management1.entity.SysRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

    @Delete("delete from sys_role_user where userId = #{userID}")
    boolean deleteByUserID(int userID);

    @Update("Update sys_role_user set roleId = 2 where roleId = #{roleId}")
    boolean changeRoleId(int roleId);

    @Select("select * from sys_role_user where roleId = #{roleId}")
    List<SysRoleUser> findByRoleId(int roleId);
}
