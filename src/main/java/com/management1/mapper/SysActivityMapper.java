package com.management1.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.management1.entity.SysActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.management1.entity.vo.activityVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
@Mapper
@Repository
public interface SysActivityMapper extends BaseMapper<SysActivity> {

    @Select("select k.*,u.username as userName from sys_activity k left join sys_user u on k.userId = u.id"
            + " ${ew.customSqlSegment}")
    List<activityVo> getActivityByPage(Page<activityVo> page, @Param(Constants.WRAPPER) QueryWrapper<activityVo> queryWrapper);

}
