package com.management1.mapper;

import com.management1.entity.SysUserGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-05-26
 */
public interface SysUserGroupMapper extends BaseMapper<SysUserGroup> {

}
