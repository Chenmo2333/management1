package com.management1.mapper;

import com.management1.entity.SysGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.management1.entity.dto.groupInfoDto;
import com.management1.entity.vo.groupInfoVo;
import com.management1.entity.vo.groupMemberVo;
import com.management1.entity.vo.groupVo;
import com.management1.entity.vo.groupVolunteerVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
public interface SysGroupMapper extends BaseMapper<SysGroup> {

    //错的语句，因为改了表结构
    @Select("select g.activityId, g.id, g.name, g.description, v.username as volunteer " +
            "from sys_group g left join sys_volunteer v " +
            "on g.activityId = v.activityId and g.id = v.groupId " +
            "where g.activityId = #{activityId} " )
    List<groupVo> getGroupInfoById(@Param("activityId") Integer activityId);

    //根据活动id，查询分组中的志愿者
    List<groupVolunteerVo> getGroupVolunteer(Integer activityId);

    //根据活动id，查询分组中的组员、组长、副组长、执行组长
    List<groupMemberVo> getGroupMemberVo(Integer activityId);

    //查询总的分组成员信息
    List<groupInfoDto> getGroupInfoList(Integer activityId);

    //测试视图，查询分组信息



}
