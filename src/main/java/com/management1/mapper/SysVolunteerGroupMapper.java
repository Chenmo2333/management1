package com.management1.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.management1.entity.SysVolunteer;
import com.management1.entity.SysVolunteerGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.management1.entity.dto.groupInfoDto;
import com.management1.entity.dto.groupInfoViewDto;
import com.management1.entity.dto.roleDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
public interface SysVolunteerGroupMapper extends BaseMapper<SysVolunteerGroup> {

    //使用视图查询分组信息
    @Select("select * from viewGroupInfo where activityId = #{activityId}")
    List<groupInfoViewDto> getGroupInfoView(Integer activityId);

    //使用视图查询分组信息
    @Select("select * from viewGroupInfo ")
    List<groupInfoViewDto> getGroupInfo();

    //使用视图查询分组信息,只查询组员和志愿者
    IPage<groupInfoViewDto> groupManage(IPage<groupInfoViewDto> page, @Param(Constants.WRAPPER) Wrapper<groupInfoViewDto> queryWrapper);

    List<roleDto> getRoleList();

    @Select("select DISTINCT roleName from sys_volunteer_group where roleId = #{roleId}")
    String getRoleName(Integer roleId);

    List<SysVolunteer> getMemberByGroupId(Integer groupId);
}
