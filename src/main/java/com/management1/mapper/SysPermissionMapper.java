package com.management1.mapper;

import com.management1.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<String> selectPermissionValueByUserId(Integer id);

    List<String> selectAllPermissionValue();

    List<SysPermission> selectPermissionByUserId(Integer userId);
}
