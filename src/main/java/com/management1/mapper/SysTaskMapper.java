package com.management1.mapper;

import com.management1.entity.SysTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-06-21
 */
public interface SysTaskMapper extends BaseMapper<SysTask> {

    @Select("select id as taskId , taskName from sys_task GROUP BY taskName")
    List<SysTask> getAllTaskName();
}
