package com.management1.mapper;

import com.management1.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-04-26
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
