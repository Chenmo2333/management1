package com.management1.mapper;

import com.management1.entity.SysGroup;
import com.management1.entity.SysVolunteer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmo
 * @since 2021-05-13
 */
public interface SysVolunteerMapper extends BaseMapper<SysVolunteer> {

    List<SysVolunteer> getMemberNotInGroup();
}
