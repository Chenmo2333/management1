package com.management1;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.management1.entity.*;
import com.management1.entity.activity.FirstActivity;
import com.management1.entity.activity.TwoGroup;
import com.management1.entity.dto.groupInfoViewDto;
import com.management1.entity.vo.finalGroupInfo;
import com.management1.entity.vo.groupInfoVo;
import com.management1.entity.vo.groupMemberVo;
import com.management1.entity.vo.groupVolunteerVo;
import com.management1.mapper.*;
import com.management1.service.SysGroupService;
import com.management1.service.SysVolunteerGroupService;
import com.management1.service.SysVolunteerService;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.*;

@SpringBootTest
class Management1ApplicationTests {

    @Resource
    private SysUserMapper userMapper;

    @Autowired
    private SysGroupService groupService;

    @Resource
    private SysRoleUserMapper roleUserMapper;

    @Resource
    private SysRoleMapper roleMapper;

    @Resource
    private SysActivityMapper activityMapper;

    @Resource
    private SysGroupMapper groupMapper;

    @Resource
    private SysVolunteerGroupMapper volunteerGroupMapper;

    @Resource
    private SysVolunteerGroupService volunteerGroupService;

    @Test
    void contextLoads() {
        //获取总的分组信息
        List<groupInfoViewDto> groupInfoView = volunteerGroupMapper.getGroupInfoView(1);
        //对信息进行分类
        Map<Integer, List<groupMemberVo>> volunteerMap = new HashMap<>();
        Map<Integer, List<groupMemberVo>> memberMap = new HashMap<>();
        Map<Integer, groupMemberVo> leadMap = new HashMap<>();
        Map<Integer, groupMemberVo> leadTwoMap = new HashMap<>();

        for (groupInfoViewDto tag : groupInfoView) {
            //定义中间变量存储各个对象
            groupMemberVo volunteerTemp = new groupMemberVo();
            groupMemberVo memberTemp = new groupMemberVo();
            groupMemberVo leaderTemp = new groupMemberVo();
            groupMemberVo leaderTwoTemp = new groupMemberVo();
            //如果该分组不存在则加入map集合的key中,如果不提前加入，则会报空指针异常
            volunteerMap.computeIfAbsent(tag.getGroupId(), k -> new ArrayList<>());
            memberMap.computeIfAbsent(tag.getGroupId(), k -> new ArrayList<>());
            //将志愿者加入对应的分组中
            if ("志愿者".equals(tag.getRoleName())) {
                volunteerTemp.setRoleName("志愿者");
                volunteerTemp.setGroupId(tag.getGroupId());
                volunteerTemp.setUserId(tag.getVolunteerId());
                volunteerTemp.setUserName(tag.getVolunteerName());
                volunteerMap.get(tag.getGroupId()).add(volunteerTemp);
            } else if ("组员".equals(tag.getRoleName())) {
                memberTemp.setRoleName("组员");
                memberTemp.setGroupId(tag.getGroupId());
                memberTemp.setUserId(tag.getVolunteerId());
                memberTemp.setUserName(tag.getVolunteerName());
                memberMap.get(tag.getGroupId()).add(memberTemp);
            } else if ("组长".equals(tag.getRoleName())) {
                leaderTemp.setGroupId(tag.getGroupId());
                leaderTemp.setRoleName("组长");
                leaderTemp.setUserId(tag.getVolunteerId());
                leaderTemp.setUserName(tag.getVolunteerName());
                leadMap.put(tag.getGroupId(), leaderTemp);
            } else if ("副组长".equals(tag.getRoleName())) {
                //将副组长加入对应分组中
                leaderTwoTemp.setGroupId(tag.getGroupId());
                leaderTwoTemp.setRoleName("副组长");
                leaderTwoTemp.setUserId(tag.getVolunteerId());
                leaderTwoTemp.setUserName(tag.getVolunteerName());
                leadTwoMap.put(tag.getGroupId(), leaderTwoTemp);
            }
        }

        //定义最终需要返回的结果集
        List<groupInfoVo> groupInfos = new ArrayList<>();
        //查询分组信息
        QueryWrapper<SysGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("activityId", 1);
        wrapper.orderByAsc("groupId");
        List<SysGroup> sysGroups = groupMapper.selectList(wrapper);
        //封装进集合
        for (SysGroup sysGroup : sysGroups) {
            groupInfoVo info = new groupInfoVo();
            List<groupMemberVo> sysVolunteers = volunteerMap.get(sysGroup.getGroupId());
            List<groupMemberVo> groupMemberVos = memberMap.get(sysGroup.getGroupId());

            info.setLeader(leadMap.get(sysGroup.getGroupId()));
            info.setLeaderTwo(leadTwoMap.get(sysGroup.getGroupId()));
            info.setActivityId(sysGroup.getGroupId());
            info.setGroupName(sysGroup.getGroupName());
            info.setDescription(sysGroup.getDescription());
            info.setVolunteers(sysVolunteers);
            info.setMembers(groupMemberVos);

            groupInfos.add(info);
        }
        System.out.println(groupInfos);
    }

    //测试自定义分页功能
    @Test
    void test() {
        Page<groupInfoViewDto> page = new Page<>(1,10);
        QueryWrapper<groupInfoViewDto> wrapper = new QueryWrapper<>();
        wrapper.eq("activityId",1);
        wrapper.eq("roleId",4).or().eq("roleId",5);
        IPage<groupInfoViewDto> groupInfoViewDtoIPage = volunteerGroupMapper.groupManage(page, wrapper);
        System.out.println(groupInfoViewDtoIPage);
    }

    //测试每个活动下的分组情况
    @Test
    void test2(){
        //先查出所有活动
        List<SysActivity> activityList = activityMapper.selectList(null);
        //再查出所有分组
        List<SysGroup> groupList = groupMapper.selectList(null);
        List<FirstActivity> list = new ArrayList<>();
        for (SysActivity activity : activityList) {
            FirstActivity firstActivity = new FirstActivity();
            firstActivity.setActivityId(activity.getId());
            firstActivity.setActivityName(activity.getName());
            List<TwoGroup> twoGroupList = new ArrayList<>();
            for (SysGroup group : groupList) {
                if (activity.getId().equals(group.getActivityId())) {
                    TwoGroup twoGroup = new TwoGroup();
                    twoGroup.setGroupId(group.getGroupId());
                    twoGroup.setGroupName(group.getGroupName());
                    twoGroupList.add(twoGroup);
                }
            }
            firstActivity.setGroups(twoGroupList);
            list.add(firstActivity);
        }
        System.out.println(list);
    }

    @Test
    public void testDelete(){
        double i = 1.1;
        System.out.println((i % 1 == 0));

    }


}
